package pl.org.pfig.singleton;

public class SingletonExample {

    private static SingletonExample _instance;

    private String name;

    private SingletonExample() {}

    public static SingletonExample getInstance() {
        // lzay initialization
        if (_instance == null) {
            _instance = new SingletonExample();
        }
        System.out.println("Zwracam instancje, bo już jest utworzona");
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
