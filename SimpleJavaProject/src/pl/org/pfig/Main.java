package pl.org.pfig;

import pl.org.pfig.factory.AnimalFactory;
import pl.org.pfig.factory.AnimalInterface;
import pl.org.pfig.factoryother.Coupon;
import pl.org.pfig.factoryother.CouponFactory;
import pl.org.pfig.fluidinterface.Beer;
import pl.org.pfig.fluidinterface.People;
import pl.org.pfig.fluidinterface.Person;
import pl.org.pfig.singleton.SingletonExample;
import pl.org.pfig.templatemethod.Laptop;
import pl.org.pfig.templatemethod.MidiTowerComputer;
import pl.org.pfig.templatemethod.PersonalComputer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        /* Singleton Pattern */
        SingletonExample se1 = SingletonExample.getInstance();
        se1.setName("pawel");
        SingletonExample se2 = SingletonExample.getInstance();
        System.out.println(se2.getName());

        /* Template Method Pattern */
        Laptop laptop = new Laptop();
        System.out.println("Laptop");
        laptop.devices();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("MidiTowerComputer");
        mtc.devices();

        PersonalComputer pc = new PersonalComputer();
        System.out.println("PersonalComputer");
        pc.devices();

        /* Factory Pattern */
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();

        AnimalInterface dog = AnimalFactory.getAnimal("dog");
        dog.getSound();

        AnimalInterface frog = AnimalFactory.getAnimal("frog");
        frog.getSound();

        //AnimalInterface tiger = AnimalFactory.getAnimal("tiger");

        /* Factory pattern 2 */
        int[] numbers = new int[6];
        for(int i = 0; i <numbers.length; i++) {
            numbers[i] = new Random().nextInt(48) + 1;
        }

        Coupon c = CouponFactory.getCoupon(numbers);

        System.out.println(c.getA());

        /* Chaining */

        Beer piwo = new Beer();
        piwo
                .setName("Special")
                .setType("Lager")
                .setTaste("Bitter")
                .setPrice(2.19);

        System.out.println(piwo);

        List<Person> people = new LinkedList<>();

        people.add(new Person("Paweł","Testowy",30));
        people.add(new Person("Paweł","Inny",42));
        people.add(new Person("Mirek","Przykladowy",22));
        people.add(new Person("Darek","Testowy",44));

        People pp = new People().addGroup("staff" , people);

        for(Person pers : pp.from("staff").name("Paweł").lastname("Testowy").get()) {
            System.out.println(pers);
        }
    }
}
