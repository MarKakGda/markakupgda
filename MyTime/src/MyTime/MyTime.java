package MyTime;

public class MyTime {
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	public MyTime(int hour, int minute, int second){
		setTime(hour, minute, second);
	}
	
	public void setTime (int hour, int minute, int second){
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}
	
	public int getHour(int hour){
		return this.hour;
		}
	
	public int getMinute(int minute){
		return this.minute;
		}
	
	public int getSecond(int second){
		return this.second;
		}
	
	public void setHour(int hour){
		if (hour >= 0 && hour < 24){
			this.hour = hour;
		}
	}
	
	public void setMinute (int minute){
		if (minute >=0 && minute < 60){
			this.minute = minute;
		}
	}
	
	public void setSecond (int second){
		if (second >=0 && second < 60){
			this.second = second;
		}
	}
	
	public MyTime nextHour() {
		int newHour = hour + 1;
		if(newHour == 24) {
			newHour = 0;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime nextMinute() {
		int newHour = hour,
			newMinute = minute + 1;
		if(newMinute == 60) {
			newMinute = 0;
			newHour++;
		}
		return new MyTime(newHour, newMinute, second);
	}
	
	public MyTime nextSecond() {
		int newHour = hour,
			newMinute = minute,
			newSecond = second + 1;
		if(newSecond == 60){
			newSecond = 0;
			newMinute++;
		}
		if(newMinute == 60){
			newMinute = 0;
			newHour++;
		}		
		return new MyTime(newHour, newMinute, newSecond);
	}
	
	public MyTime prevHour() {
		int newHour = hour - 1;
		if(hour == 0) {
			newHour = 24;
			newHour = newHour - 1;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime prevMinute() {
		int newHour = hour,
			newMinute = minute - 1;
			if(minute == 0) {
				newMinute = 60;
				newMinute = newMinute - 1;
			}
				if (hour == 0){
				newHour = 24;
				newHour = newHour - 1;
			}
			return new MyTime(newHour, newMinute, second);
		}
	
	public MyTime prevSecond() {
		int newHour = hour,
			newMinute = minute,
			newSecond = second - 1;

			if(second == 0){
				newSecond = 60;
				newSecond = newSecond - 1;
			}
			if(newMinute == 0){
				newMinute = 60;
				newMinute = newMinute - 1;
			}
			if(newHour == 0){
				newHour = 24;
				newHour = newHour -1;
			}
				
			
			return new MyTime(newHour, newMinute, newSecond);
		}
	
	
	public String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}
	}
	
	public String toString(){
		return leadZero(this.hour)+":"
			  +leadZero(this.minute)+":"
			  +leadZero(this.second);
	}
	
}
	
	
