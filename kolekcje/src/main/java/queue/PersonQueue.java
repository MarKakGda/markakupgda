package queue;

//import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Comparator;

public class PersonQueue {
	
		public static void main(String[] args) {
			
		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>(){
			@Override
			public int compare(Person o1, Person o2){
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		persons.offer(new Person ("Jan Kowalski",25));
		persons.offer(new Person ("Adam Nowak",25));
		persons.offer(new Person ("Magdalena Kowalczyk",27));
		persons.offer(new Person ("Anna Kwiatkowska",21));
		
		while(!persons.isEmpty()) {
			
			System.out.println(persons.poll());
		}
	}
	
	/*public static void showPersons(Queue<Person> persons, int age){
		Person person = persons.get(age);
		System.out.println(person.getName());
	}*/

}
