package queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortowanieListy {

	public static void main(String[] args) {
		
		List<Person> persons = new ArrayList<>();
		
		persons.add(new Person ("Jan Kowalski",25));
		persons.add(new Person ("Adam Nowak",25));
		persons.add(new Person ("Magdalena Kowalczyk",27));
		persons.add(new Person ("Anna Kwiatkowska",21));
		
		System.out.println("Przed sortowaniem");
		for (Person person : persons){
			System.out.println(person);
		}
		
		Collections.sort(persons);
		
		System.out.println("Po sortowaniu");
		for (Person person : persons){
			System.out.println(person);
		}
		
		Collections.shuffle(persons);
		
		System.out.println("Po tasowaniu");
		for (Person person : persons){
			System.out.println(person);
		}
	}

}
