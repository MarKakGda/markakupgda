package kolekcje;

import java.util.ArrayList;
import java.util.List;

public class UserList {
	
	public static void main(String[] args) {
		
		List<User> users = new ArrayList<>();
		users.add(new User("admin" , "admin"));
		users.add(new User("Stefan" , "12345"));
		
		for (User user : users) {
			System.out.println("Username : "+user.getUsername()+"\nPassword : "+user.getPassword());
		}	
	}
}
