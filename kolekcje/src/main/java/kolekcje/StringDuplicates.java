package kolekcje;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {
	
	public static boolean containDuplicates(String text) {
	
	Set<Character> set = new HashSet<>();
	
	for(char i : text.toCharArray()) {
		set.add(i);
		}
	return set.size() != text.length();
	}
}

