package kolekcje;

import java.util.List;

public class ListMath {
	
	public static int sum(List<Integer> numbers){
		int sum = 0;
		for(int i : numbers) {
			sum += i;
		}
		return sum;
	}
	
	public static int sumByFor(List<Integer> numbers){
		int sum = 0;
		for(int i = 0; i < numbers.size(); i++ ){
			sum += numbers.get(i);
			
		}
	return sum;
	}
	
	public static int product(List<Integer> numbers){
		int product = 1;
		for(int i : numbers) {
		product *= i;
		}
		return product;
	}
	
	public static int productByFor(List<Integer> numbers){
		int product = 1;
		for(int i = 0; i < numbers.size(); i++ ){
			product *= numbers.get(i);
			
		}
	return product;
	}
	
	public static double Aritmetic(List<Integer> numbers){
		return (double) sum(numbers) / numbers.size(); 
	}
}
