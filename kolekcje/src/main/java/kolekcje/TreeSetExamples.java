package kolekcje;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExamples {
	
	public static void main(String[] args) {
		TreeSet<Integer> treeset = new TreeSet<>(Arrays.asList(1,2,3,4,5,6));
		System.out.println("Ca�y zbi�r : ");
		for (int integer : treeset){
			System.out.println(integer);
		}
		System.out.println("Ca�y headset(3) : ");
		for(Integer integer : treeset.headSet(3)){
			System.out.println(integer);
		}
		System.out.println("Ca�y tailset(3) : ");
		for (Integer integer : treeset.tailSet(3)){
			System.out.println(integer);
		}
	}

}
