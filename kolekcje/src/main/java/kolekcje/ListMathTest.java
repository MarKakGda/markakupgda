package kolekcje;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {
	
	@Test
	public void sumTest() {
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		assert ListMath.sum(numbers) == 15;

 	}
	
	@Test
	public void sumByForTest() {
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		
		assert ListMath.sumByFor(numbers) == 15;
 	}
	
	@Test
	public void productTest() {
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		assert ListMath.product(numbers) == 120;

 	}
	
	@Test
	public void productByForTest() {
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		
		assert ListMath.productByFor(numbers) == 120;
 	}
	
	@Test
	public void Aritmetic(){
		List<Integer> numbers = new ArrayList();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		
		assert ListMath.Aritmetic(numbers) == 5.0;
		
	}

}
