package kolekcje;

import java.util.Arrays;
import java.util.List;

public class ListaTablica {
	
	public static void main(String[] args) {
		int[] array = {4,2,2,1,5,29,3,8};
		List<Integer> list = Arrays.asList(1,2,4,2,5,12,3,2);
		
		System.out.println("Wynik : "+countDuplicates(array, list));
		
	}
	
	public static int countDuplicates(int[] array, List<Integer> list){
		int count = 0;
		
		//if (array.length == list.size()){
		//	throw new RuntimeException("Kolekcje s� r�nej d�ugo�ci."+"Tablica : "+array.length+"Lista : "+list.size());
		//}
		
		int elements = Math.min(array.length, list.size());
		for(int i = 0; i < elements; i++){
			if(array[i] == list.get(i)){
				count++;
			}
		}	
		return count;
	}
}
