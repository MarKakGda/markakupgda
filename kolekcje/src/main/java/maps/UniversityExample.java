package maps;

public class UniversityExample {

	public static void main(String[] args) {
		
		University uni = new University();
		
		uni.addStudent(100200, "Jan" , "Kowalski");
		uni.addStudent(100300, "Adam" , "Nowak");
		uni.addStudent(100400, "Anna" , "Martyniuk");
		uni.addStudent(100500, "Teresa" , "Kowalczyk");
		
		System.out.println(uni.studentExists(100400));
		System.out.println(uni.getStudents(100400).getName());
		System.out.println(uni.studentsNumber());
		uni.showAll();
	}

}
