package maps;

import java.util.HashMap;
import java.util.Map;

public class StudentsExamples {

	public static void main(String[] args) {
		
		
		Map<Integer,Students> students = new HashMap<>();
		students.put(100200, new Students(100200, "Jan", "Kowalski"));
		students.put(101200, new Students(101200, "Adam", "Nowak"));
		students.put(102200, new Students(102200, "Anna", "Kaczorowska"));
		students.put(103200, new Students(103200, "Magdalena", "Andrzejewicz"));
		
		
		for(Students student : students.values()){
			System.out.println(student.getIndexnumber() + " -> "+student.getName() + " " + student.getSurname());
		}
		
		System.out.println(students.containsKey(100200));
		System.out.println(students.containsKey(100104));
		System.out.println(students.size());		
			
	}
}
