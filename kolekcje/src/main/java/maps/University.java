package maps;

import java.util.HashMap;
import java.util.Map;

public class University {
	private Map<Integer, Students> students = new HashMap<>();
	
	public void addStudent(int indexnumber, String name, String surname){
		Students student = new Students(indexnumber, name, surname);
		students.put(student.getIndexnumber(), student);
	}
	
	public boolean studentExists(int indexnumber) {
		return students.containsKey(indexnumber);
	}
	
	public Students getStudents(int indexnumber){
		return students.get(indexnumber);
	}
	
	public int studentsNumber(){
		return students.size();
	}
	
	public void showAll() {
		for (Students s : students.values()){
			System.out.println(s.getIndexnumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}
}
