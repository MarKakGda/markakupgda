package pl.org.pfig.main;

import java.util.Random;

public class Main {
	public static void main(String[] args) {
		
		int[][] testArray = { { 49, 2, 3},
				{4, 7},
				{21, 13, 9, 33, 55, 66 } };
		
		TwoDArrays td = new TwoDArrays();
		
		System.out.println("Suma element�w podzielnych przez 7 w tablicy to : " +td.addElementsDividedBySeven(testArray));
		
		
		
		//StringUtil su = new StringUtil("przykladowy tekst");
		//su.print().cut(2,5).print();
		
		
		System.exit(0);
		
		
		
		
		String myStr = "  przykladowy ciag znakow  ";
		
		if(myStr.equals("  przykladowy ciag znakow  ")) {
			System.out.println("Ci�gi s� takie same.");
		}
		
		if(myStr.equalsIgnoreCase("  PRZYKLADOWY ciag znaKOW  ")) {
			System.out.println("Ci�gi znak�w s� takie same, bez badania wielko�ci liter.");
		}
		
		System.out.println("dlugosc myStr to: " + myStr.length());
		
		System.out.println(myStr.substring(14));
		
		String otherStr = "MARCIN";
		
		System.out.println(otherStr.substring(1));
		
		System.out.println(otherStr.substring(1,4));
		
		System.out.println(otherStr.substring(1, otherStr.length() - 1));
		
		System.out.println(myStr.trim());
		
		//myStr = myStr.trim();
		
		System.out.println(myStr.charAt(2) + " " + myStr.charAt(3));
		
		String alphabet = "";
		for ( char c = 97; c <= 122; c++){
			alphabet += c;
		}
		
		System.out.println(alphabet);
		
		System.out.println( myStr.replace("ciag",  "lancuch"));
		
		System.out.println(myStr.concat(otherStr));
		
		System.out.println(myStr);
		
		if(myStr.contains("klad")) {
			System.out.println("Slowo klad zawiera sie w " + myStr);
		}
		
		if(alphabet.startsWith("a")) {
			System.out.println("alfabet zaczyna sie od A");
		}
		if(alphabet.endsWith("z")) {
			System.out.println("alfabet konczy sie na Z");
		}
		
		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));
		System.out.println(myStr.substring(myStr.indexOf("a"), myStr.lastIndexOf("a")));
		
		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for(String s: arrOfStr) {
			System.out.println("\t" + s);
			
		Random r = new Random();
		System.out.println(r.nextInt());
		System.out.println(r.nextInt(15));
		
		int a = 10;
		int b = 25;
		
		System.out.println(a + r.nextInt(b-a));
		
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c + "");
		
		HTMLExercise he = new HTMLExercise("To jest m�j tekst.");
		
		he.print().strong().print().p().print();
		
		}
		
		}

}
