package pl.org.pfig.main;

public class TwoDArrays {
	public int addElementsDividedBySeven(int[][] arr){
		int sum = 0;
		int rows = arr.length;
		for(int i = 0; i < rows; i++){
			int cols = arr[i].length;
			for(int j = 0; j < cols; j++){
				if(arr[i][j] % 7 == 0){
					sum += arr[i][j];
				}
				
			}
		}
		return sum;
	}
}
