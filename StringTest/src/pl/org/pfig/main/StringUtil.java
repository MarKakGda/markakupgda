package pl.org.pfig.main;

public class StringUtil {
	private String str;

	public StringUtil(String str) {
		this.str = str;
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil append(String arg) {
		str = str + arg;
		return this;
	}

	public StringUtil letterSpacing() {
		String[] arr = str.split("");
		String ret = "";
		for (String c : arr) {
			ret = c + " ";
			System.out.print(ret);
		}
		System.out.println("");
		return this;
	}
	public StringUtil reverse(){
		String[] arr = str.split("");
		String ret = "";
		for (int c = arr.length - 1 ; c>=0; c--){
			ret = arr[c] + "";
			System.out.print(ret);

		}
		System.out.println("");
		return this;
	}
	
	public StringUtil getAlphabet(){
	String alphabet = "";
	for ( char c = 97; c <= 122; c++){
		alphabet += c;
	}
	System.out.println(alphabet);
	return this;
	}
	
	public StringUtil getFirstLetter(){
		System.out.println(str.charAt(0));
	return this;
	}
	
	public StringUtil limit(int n){
		String[] arr = str.split("");
		String ret = "";
		for (int c = 0; c <= arr.length ; c++){
			if(c <= n){
			ret = arr[c] + "";
			System.out.print(ret);
			}
		}
		System.out.println("");
		return this;
	}
	
	public StringUtil insertAt(String string, int n){
		System.out.println(str.substring(0,n)+" "+string+" "+str.substring(n));
		return this;
	}
	
	public StringUtil resetText(){
		str = "";
		return this;
	}
	
	public StringUtil swapLetters(){
		str = str.charAt(str.length()  -1) + str.substring(1, str.length() -1) + str.charAt(str.length() - 1);
		return this;
	}
	
	public StringUtil createSentence() {
				str = (str.charAt(0) + "").toUpperCase() + str.substring(1);
				if(str.charAt(str.length() - 1) != '.') {
				str += ".";
		}
		return this;
		}
	
	public StringUtil cut(int from, int to) {
			String newString = "";
			for(int i = from; i <= to; i++) {
				newString += str.charAt(i);
			}
			str = newString;
			return this;
		}
		
	
}
