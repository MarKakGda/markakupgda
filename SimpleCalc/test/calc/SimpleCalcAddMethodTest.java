package calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		double a = 3.1567576, b = 6.9;
		assertEquals(10.0, sc.add(a, b), 0.01);
	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenNegativeNumberAreSum() {
		int a = 3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		//sc.add(a, b);
		assertEquals(-9, sc.add(-a, -b));
	}
	
	@Test
	public void whenFirstPositiveNumberAddSecondNegativeNumberAreGivenPositiveNumberAreSum() {
		int a = 3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		//sc.add(a, b);
		assertEquals(3, sc.add(-a, b));
	}
	
	@Test
	public void whenFirstNegativeNumberAddSecondPositiveNumberAreGivenNegativeNumberAreSum() {
		int a = 3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		//sc.add(a, b);
		assertEquals(-3, sc.add(a, -b));		
	}
	
	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSumOutOfRange() {
		int a = 1073741824, b = 1073741824;
		SimpleCalc sc = new SimpleCalc();
		//sc.add(a, b);
		assertEquals(300000000, sc.add(a, b));
	}
	
	@Test
	public void whenTwoMaxIntegerAreGivenPositiveNumberAreExpected() {
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) > 0);
	}
	
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		SimpleCalc sc = new SimpleCalc();
		sc.exThrow();
	}

}
