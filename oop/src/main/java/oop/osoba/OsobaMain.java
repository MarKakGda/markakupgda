package oop.osoba;

public class OsobaMain {

	public static void main(String[] args) {
		Osoba jan = new Osoba("Jan",23);
		jan.przedstawSie();
		Osoba adam = new Osoba("Adam",25);
		adam.przedstawSie();
		adam.setImie("Jacek");
		adam.przedstawSie();
		
		System.out.println("Imi� Adama to : "+ adam.getImie());
}
}