package oop.teddybear;

public class TeddyBear {

	private String name;
	private String color;
	private int hands;
	
	public TeddyBear(String name, String color, int hands){
		this.name = name;
		this.color = color;
		this.hands = hands;
	}
	
	public void NameOfTeddy() {
		System.out.println(this.name+" "+this.color+" "+this.hands);
	}
}
