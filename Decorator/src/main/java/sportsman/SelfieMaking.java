package sportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class SelfieMaking implements Sportsman{
    private Sportsman sportsman;

    public SelfieMaking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare() {
        sportsman.prepare();
        System.out.println("Robię selfie.");
    }

    public void doPumps(int number) {
        sportsman.doPumps(number);
        System.out.println("Robię selfie.");
    }

    public void doSquats(int number) {
        sportsman.doSquats(number);
        System.out.println("Robię selfie.");
    }

    public void doCrunches(int number) {
        sportsman.doCrunches(number);
        System.out.println("Robię selfie.");
    }
}
