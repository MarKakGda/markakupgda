package sportsman;

public class DoubleSeries implements Sportsman {
    private Sportsman sportsman;

    public DoubleSeries(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare() {
        sportsman.prepare();
        System.out.println("Podwójna seria.");
    }

    public void doPumps(int number) {
        sportsman.doPumps(number);
        sportsman.doPumps(number);
    }

    public void doSquats(int number) {
        sportsman.doSquats(number);
        sportsman.doSquats(number);
    }

    public void doCrunches(int number) {
        sportsman.doCrunches(number);
        sportsman.doCrunches(number);
    }

}
