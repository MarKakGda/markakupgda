package sportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class FitnessStudio {
    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(2);
        sportsman.doSquats(3);
        sportsman.doCrunches(5);
    }
}
