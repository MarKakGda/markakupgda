package sportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {
    public static void main(String[] args) {
        Sportsman sportsman = new NoPrepare(new SelfieMaking(new WaterDrinking(new DoubleSeries(new BasicSportsman()))));

        FitnessStudio fitnessStudio = new FitnessStudio();
        fitnessStudio.train(sportsman);
    }
}
