package sportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class NoPrepare implements Sportsman {
    private Sportsman sportsman;

    public NoPrepare(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare()  {
        System.out.println("Brak rozgrzewki.");
    }

    public void doPumps(int number) {
        sportsman.doPumps(number);
    }

    public void doSquats(int number) {
        sportsman.doSquats(number);
    }

    public void doCrunches(int number) {
        sportsman.doCrunches(number);

    }
}
