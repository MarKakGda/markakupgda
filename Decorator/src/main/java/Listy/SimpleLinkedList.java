package Listy;



import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedList implements SimpleList {

    private Element first;
    private Element last;
    private int number = 0;


    public void add(int value) {
        Element e = new Element();
        e.value = value;
        if (number == 0) {
            first = e;
        } else {
            last.next = e;
            e.prev = last;
        }
        last = e;
        number++;
    }


    public int get(int index) {
        if (index < number) {
            Element e = first;
            for (int i = 0; i < index; i++) {
                e = e.next;
            }
            return e.value;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }


    public void add(int value, int index) {
        if(index < number){
        Element newElement = new Element();
        newElement.value = value;
        Element a =  first;
            for(int i = 0; i < index ; i++) {
                a =  a.next;
            }
            newElement.next = a;
            newElement.prev = a.prev;
            a.prev.next = newElement;
            a.prev = newElement;
            number++;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }


    public boolean contain(int value) {
       Element e = first;
        while (e != null){
            if ( e.value == value){
                return true;
            }
            e = e.next;
        }
        return false;
    }



    public void remove(int index) {
        if (index > number) {
            throw new IndexOutOfBoundsException();
        }
        Element a = first;
        for (int i = 0; i < index; i++) {
            a = a.next;
        }
        if(index < number-1 && index != 0) {
            a.prev.next = a.next;
            a.next.prev = a.prev;
        } else if(number == 1) {
            first = null;
            last = null;
        }else if (index == number-1) {
            last = a.prev;
            last.next = null;
        } else if(index == 0) {
            first = a.next;
            first.prev = null;
        }
        number--;
    }


    public void removeValue(int value) {
        Element a =  first;
        for(int i = 0; i < number ; i++) {
            if(a.value == value) {
                remove(i);
            }
            a =  a.next;
        }
    }


    public int size() {
        return number;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;
    }
}
