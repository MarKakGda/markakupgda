package person;


import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class FilePrinter {

    private final PersonPrinter personPrinter;


    public FilePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void printToFile(Person person, String filename) {
        try (PrintStream out = new PrintStream(filename)) {
            personPrinter.print(person, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
