package person;



import static person.PersonPrinterBuilder.personWho;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

        //PersonPrinter personPrinter = new EyesColorPrinter(new WeightPrinter(new HeightPrinter(new AgePrinter(new BasicDataPrinter()))));

        PersonPrinter personPrinter = personWho()
                .printAge()
                .printHeight()
                .printWeight()
                .printEyesColor()
                .getHim();


        Person person = new Person("Jan" , "Kowalski" , 20, 81.5 , 175, Person.EyesColor.BLUE);
        personPrinter.print(person, System.out);

        FilePrinter filePrinter = new FilePrinter(personPrinter);
        filePrinter.printToFile(person, "person.txt");

    }
}
