package person;

/**
 * Created by RENT on 2017-06-23.
 */
public class PersonPrinterBuilder {

    private PersonPrinter personPrinter = new BasicDataPrinter();

    public PersonPrinterBuilder printAge() {
        personPrinter = new AgePrinter(personPrinter);
        return this;
    }

    public PersonPrinterBuilder printWeight() {
        personPrinter = new WeightPrinter(personPrinter);
        return this;
    }

    public PersonPrinterBuilder printHeight() {
        personPrinter = new HeightPrinter(personPrinter);
        return this;
    }

    public PersonPrinterBuilder printEyesColor() {
        personPrinter = new EyesColorPrinter(personPrinter);
        return this;
    }

    public static PersonPrinterBuilder personWho() {
        return new PersonPrinterBuilder();
    }

    public PersonPrinter getHim() {
        return personPrinter;
    }
}
