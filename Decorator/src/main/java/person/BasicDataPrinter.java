package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter {

    private PersonPrinter personPrinter;

    public void print(Person person, PrintStream out) {
        out.println("Name: "+person.getName());
        out.println("Surname: "+person.getSurname());
    }
}
