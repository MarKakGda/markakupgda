package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class HeightPrinter implements PersonPrinter{

    private PersonPrinter personPrinter;

    public HeightPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void print(Person person, PrintStream out) {
        personPrinter.print(person, out);
        out.println("Height : "+person.getHeight());
    }
}
