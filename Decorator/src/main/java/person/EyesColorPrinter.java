package person;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class EyesColorPrinter implements PersonPrinter{

    private PersonPrinter personPrinter;

    public EyesColorPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void print(Person person, PrintStream out) {
        personPrinter.print(person, out);
        out.println("Eyes color : "+person.getEyesColor());
    }
}
