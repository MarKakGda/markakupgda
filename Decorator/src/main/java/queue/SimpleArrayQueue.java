package queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */

public class SimpleArrayQueue implements SimpleQueue {

    private int[] data = new int[8];
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if (start == end && !isEmpty) {
            int[] newData = new int[data.length*2];
            int i = 0;
            while (!isEmpty) {
                newData[i] = poll();
                i++;
            }
            data = newData;
            start = 0;
            end = i;
        }
        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    @Override
    public int poll() {
        if (!isEmpty) {
            int result = data[start];
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }


    @Override
    public int peek() {
        if (!isEmpty) {
            return data[start];
        } else {
            throw new NoSuchElementException();
        }
    }
}
