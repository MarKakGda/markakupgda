package queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue implements SimpleQueue {

    private Element first;
    private Element last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int value) {
        Element e = new Element();
        e.value = value;
        if (isEmpty()) {
            first = e;
            last = e;
        } else {
            last.next = e;
            last = e;
        }
    }

    @Override
    public int poll() {
            int result = peek();
            first = first.next;
            if(first == null) {
                last = null;
            }
            return result;
    }

    @Override
    public int peek() {
        if (!isEmpty()) {
            return first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public static class Element {
        int value;
        Element next;
    }
}
