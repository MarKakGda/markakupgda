package queue.generic;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */

public class SimpleArrayQueue<T> implements SimpleQueue<T> {

    private Object[] data = new Object[8];
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(T value) {
        if (start == end && !isEmpty) {
            Object[] newData = new Object[data.length*2];
            int i = 0;
            while (!isEmpty) {
                newData[i] = poll();
                i++;
            }
            data = newData;
            start = 0;
            end = i;
        }
        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    @Override
    public T poll() {
        if (!isEmpty) {
            T result = (T) data[start];
            data[start] = null;
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }


    @Override
    public T peek() {
        if (!isEmpty) {
            return (T) data[start];
        } else {
            throw new NoSuchElementException();
        }
    }
}
