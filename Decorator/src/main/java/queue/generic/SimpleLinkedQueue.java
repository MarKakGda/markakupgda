package queue.generic;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue<T> implements SimpleQueue<T> {

    private Element<T> first;
    private Element<T> last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(T value) {
        Element e = new Element();
        e.value = value;
        if (isEmpty()) {
            first = e;
            last = e;
        } else {
            last.next = e;
            last = e;
        }
    }

    @Override
    public T poll() {
            T result = peek();
            first = first.next;
            if(first == null) {
                last = null;
            }
            return result;
    }

    @Override
    public T peek() {
        if (!isEmpty()) {
            return first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public static class Element<T> {
        T value;
        Element<T> next;
    }
}
