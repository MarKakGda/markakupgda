package Queue;

import queue.SimpleQueue;
import org.junit.Test;
import queue.SimpleArrayQueue;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueueTest {
    SimpleQueue underTest1 = new SimpleArrayQueue();

    @Test
    public void offerAnPoll3Elements1() {
        underTest1.offer(2);
        underTest1.offer(4);
        underTest1.offer(6);
        underTest1.offer(8);

        assertThat(underTest1.isEmpty()).isFalse();
        assertThat(underTest1.poll()).isEqualTo(2);
        assertThat(underTest1.poll()).isEqualTo(4);
        assertThat(underTest1.poll()).isEqualTo(6);
        assertThat(underTest1.poll()).isEqualTo(8);
        assertThat(underTest1.isEmpty()).isTrue();
    }

    @Test
    public void offerPeekAndPool2Times1() {
        underTest1.offer(2);
        underTest1.peek();
        underTest1.poll();
        underTest1.offer(6);

        assertThat(underTest1.peek()).isEqualTo(6);
        assertThat(underTest1.poll()).isEqualTo(6);
        assertThat(underTest1.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements1() {
        for (int i = 0; i < 100; i++) {
            underTest1.offer(i);
        }
        for (int i = 0; i < 100; i++) {
            assertThat(underTest1.poll()).isEqualTo(i);
        }
    }
}