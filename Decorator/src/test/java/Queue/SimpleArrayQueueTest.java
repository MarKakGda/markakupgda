package Queue;


import queue.SimpleQueue;
import org.junit.Test;
import queue.SimpleArrayQueue;

import static org.assertj.core.api.Assertions.assertThat;
/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleArrayQueueTest {

    SimpleQueue underTest = new SimpleArrayQueue();

    @Test
    public void offerAnPoll3Elements() {
        underTest.offer(2);
        underTest.offer(4);
        underTest.offer(6);
        underTest.offer(8);

        assertThat(underTest.isEmpty()).isFalse();
        assertThat(underTest.poll()).isEqualTo(2);
        assertThat(underTest.poll()).isEqualTo(4);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.poll()).isEqualTo(8);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerPeekAndPool2Times() {
        underTest.offer(2);
        underTest.peek();
        underTest.poll();
        underTest.offer(6);

        assertThat(underTest.peek()).isEqualTo(6);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements() {
        for (int i = 0; i < 100; i++) {
            underTest.offer(i);
        }
        for (int i = 0; i < 100; i++) {
            assertThat(underTest.poll()).isEqualTo(i);
        }
    }
}