package pl.org.pfig.animals;

import java.util.*;

public class Animal {
	
	private List<String> animals = new LinkedList<>();
	
	public Animal() {}
	
	public void addAnimal(String animal, AnimalInterface ai) {
		//String[] animals = ai.add(animal);
		for(String a : ai.make(animal)){
			animals.add(a);
		}	
	}
	
	public String getAnimal(int index) {
		return (animals.size() > index) ? animals.get(index) : "";
		
	}
	
	public boolean containsAnimal(String animal, AnimalInterface ai) {
		String[] exists = ai.make(animal);
		for(String a : exists){
			if(!this.animals.contains(a)){
				return false;
			}
		}
		return true; 
		//return animals.contains(animal);

	}

}
