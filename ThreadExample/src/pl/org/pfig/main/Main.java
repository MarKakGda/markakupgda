package pl.org.pfig.main;

import java.util.LinkedList;
import java.util.List;

import company.Company;
import company.Employee;
import pl.org.pfig.animals.Animal;
import pl.org.pfig.animals.AnimalInterface;
import pl.org.pfig.calc.Calc;
import pl.org.pfig.calc.CalculatiorInterface;

public class Main {
	public static void main(String[] args) {
		
		List<Employee> listOfEmps = new LinkedList<>();
		
		listOfEmps.add(new Employee("Paweł","Testowy",18,3000));
		listOfEmps.add(new Employee("Piotr","Przykładowy",32,10000));
		listOfEmps.add(new Employee("Julia","Doe",41,4300));
		listOfEmps.add(new Employee("Przemysław", "Wietrak",56,4500));
		listOfEmps.add(new Employee("Zofia","Zaspa",37,3700));
		
		Company comp = new Company();
		
		comp.setEmployees(listOfEmps);
		
		comp.filter(n -> n.startsWith("P"), t -> t.toLowerCase());
		
		
	
			
		/*Thread t = new Thread(new PrzykladowyWatek());
		
		t.start();
		
		new Thread(new InnyWatek(), "Jakis Watek").start();
		System.out.println("KONIEC");
		
		new Thread(new Runnable() {
			public void run() {
				System.out.println("Pochodzę z kodu Runnable");
				
			}
		}).start();
		
		new Thread( () -> {
			int a = 1, b = 3;
			System.out.println("Suma wynosi : "+(a+b));
		}).start();


		new Thread( () -> System.out.println("Wiadomosc z FI")  ).start();
		
		
		List<Person> p = new LinkedList();
		
		p.add(new Person(1));
		
		Calc c = new Calc();
		
		//System.out.println(c.add (3,4));
		
		System.out.println(c.oper(5, 6, new CalculatiorInterface() {
			
			@Override
			public double doOperation(int a, int b) {
				return a + b;
			}
		}) );
		
		System.out.println( c.oper(17, 3, (a, b) -> ( a - b ) ) );
		System.out.println( c.oper(17, 3, (a, b) -> ( a * b ) ) );
		
		Animal a = new Animal();
		//a.addAnimal("slon");
		
		//.out.println(a.containsAnimal("slon"));
		
		a.addAnimal("slon", (anims) -> (anims.split(" ")));
		a.addAnimal("pies|kot|zaba", (anims) -> (anims.split("\\|")));
		a.addAnimal("aligator_waz", (anims) -> (anims.split("_")));
		
		a.addAnimal("katp", (anims) -> {
			String[] ret = new String[1];
			ret[0] = "";
			for(int i = anims.length() - 1; i>=0; i--){
				ret[0] += anims.charAt(i) + "";
			}
			return ret;
		});
	
		
		System.out.println(a.containsAnimal("ptak|kot", (s) -> s.split("\\|")));
		//System.out.println(a.containsAnimal("ptak"));
		}
*/
	}
}
