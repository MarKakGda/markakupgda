package pl.org.pfig.main;

public class WrongAgeException extends Exception {
	
	public WrongAgeException() throws Exception{
		throw new WrongAgeException("Wrong age exception");
		
	}
	
	public WrongAgeException(String message) throws Exception{
		super(message);
	}

}
