package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {
	
	public double readDouble() {
		double in = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb� typu double: ");
		try {
			in = sc.nextDouble();
		} catch(InputMismatchException e) {
		}
		return in;
				
	}
	
	public int readInt() {
		int in = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb� typu int: ");
		try {
			in = sc.nextInt();
		} catch(InputMismatchException e) {
		}
		return in;
				
	}
	
	public String readString() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj string: ");
		return sc.nextLine();
				
	}


}
