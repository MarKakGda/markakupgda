package pl.org.pfig.main;

public class ExceptionSimpleClass {
	public void make(int a) throws IllegalArgumentException, Exception {
		if(a == 55) {
			throw new IllegalArgumentException("Niepoprawny argument");
		}
		else if(a == 5) {
			throw new Exception("a jest r�wne 5!");
		} else {
			System.out.println("a jest poprawn� warto�ci�");
		}
	}
	
	public void exceptionExample(String name) throws Exception {
		if(name.equalsIgnoreCase("pawel")) {
			throw new PawelException("Jestes Pawlem!");
		}
	}


}
