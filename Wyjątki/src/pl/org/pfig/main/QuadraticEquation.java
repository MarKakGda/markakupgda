package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadraticEquation {
	
	private int a = 0;
	private int b = 0;
	private int c = 0;
	
	public QuadraticEquation(){
		
	}
	
	public QuadraticEquation(int a, int b, int c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	
	
	private int getNumber(String what) {
		int in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczbe  " + what + ": ");
		try {
			in = sc.nextInt();
		} catch (InputMismatchException e){
			in = 0;
		}
		return in;
		}
		
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
		public int getC() {
			return c;
		}
		public void setC(int c) {
			this.c = c;
		}
	
	public double[] solve() throws ArithmeticException{
		 double x1, x2, delta;
		 while(a== 0 && b == 0 && c == 0)
		 a = getNumber("a");
		 b = getNumber("b");
		 c = getNumber("c");
		
		 
		 delta = b*b - 4*a*c;
		 if (delta >= 0) {
		 x1 = (Math.sqrt(delta) - b) / (2*a);
		 x2 = (-Math.sqrt(delta) - b) / (2*a);
		 } else {
			 throw new ArithmeticException("Delta ujemna!");
			 
		 }
		 return new double[] {x1 , x2};
		 }		
	
}
