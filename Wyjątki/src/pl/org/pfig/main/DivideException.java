package pl.org.pfig.main;

public class DivideException extends Exception {
	public static double divide(int a, int b){
		if (b == 0){
			throw new IllegalArgumentException("Kretynie nie dzieli si� przez 0");
		} else {
			return a/b;
		}
	}
	public static double divide(double a, double b){
		if (b ==0) {
			throw new IllegalArgumentException("Kretynie nie dzieli si� przez 0");
		} else {
			return a/b;
		}
	}

}
