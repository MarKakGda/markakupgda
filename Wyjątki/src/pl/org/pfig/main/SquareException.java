package pl.org.pfig.main;

public class SquareException extends Exception {
	public static double square(int n){
		if(n < 0) {
			throw new IllegalArgumentException("Warto�� podana jest ujemna");
		} else {
			return Math.sqrt(n);
		}

	}
}
