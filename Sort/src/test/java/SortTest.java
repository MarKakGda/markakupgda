import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-28.
 */
public class SortTest {


    @Test
    public void sortowanieBabelkowe() throws Exception {
        int[] actual = {1, 7, 2, 4, 3, 8, 9, 6};
        int[] expected = {1, 2, 3, 4, 6, 7, 8, 9};
        Sort.sortowanieBabelkowe(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortowaniePrzezWstawianie() throws Exception {
        int[] actual = {5, 3, 4, 8, 1};
        int[] expected = {1, 3, 4, 5, 8};
        Sort.sortowaniePrzezWstawianie(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortowaniePrzezWymiane() throws Exception {
        int[] actual = {5, 3, 4, 8, 1};
        int[] expected = {1, 3, 4, 5, 8};
        Sort.sortowaniePrzezWymiane(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void merge() throws Exception {
        int[] left = {1, 5, 8};
        int[] right = {2, 3, 6};
        int[] result = new int[6];
        Sort.merge(left,right,result);
        Assertions.assertThat(result).containsExactly(1,2,3,5,6,8);
    }
    @Test
    public void mergeSort() throws Exception {
        int[] data = {1,4,8,2,5,6};
        Sort.mergeSort(data);
        Assertions.assertThat(data).isSorted();
    }
}