/**
 * Created by RENT on 2017-06-28.
 */
public class Sort {

    public static void sortowanieBabelkowe(int[] tab) {

        int buffer = 0;
        for (int i = tab.length - 1; i >= 0; i--) {
            for (int j = tab.length - 1; j >= 0; j--) {
                if (tab[i] > tab[j]) {
                    buffer = tab[i];
                    tab[i] = tab[j];
                    tab[j] = buffer;
                }
            }
        }
    }

    public static void sortowaniePrzezWstawianie(int[] tab) {
        int buffer = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < i; j++) {
                if (tab[i] < tab[j]) {
                    buffer = tab[i];
                    tab[i] = tab[j];
                    tab[j] = buffer;
                }
            }
        }
    }

    public static void sortowaniePrzezWymiane(int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            int min = tab[i];
            int minIndex = i;
            for (int j = i + 1; j < tab.length; j++) {
                if (min > tab[j]) {
                    min = tab[j];
                    minIndex = j;
                }
            }
            int buffer = tab[i];
            tab[i] = min;
            tab[minIndex] = buffer;
        }
    }

    public static void mergeSort(int[] data){
        if(data.length > 1){
            int middle = data.length / 2;
            int[] left = new int[middle];
            int[] right = new int[data.length - middle];
            copy(0, middle-1, data, left);
            copy(middle, data.length-1, data, right);
            mergeSort(left);
            mergeSort(right);
            merge(left,right,data);
        }
    }

    public static void merge(int[] left, int[] right, int[] data) {
        int a = 0;
        int b = 0;
        int i = 0;
        while (a < left.length && b < right.length) {
            if (left[a] <= right[b]) {
                data[i] = left[a];
                a++;
            } else {
                data[i] = right[b];
                b++;
            }
            i++;
        }
        while (a < left.length) {
            data[i++] = left[a++];
        }
        while (b < right.length) {
            data[i++] = right[b++];
        }
    }


    public static void copy(int start, int end, int[] data, int[] copy) {
        int j = 0;
        for(int i = start ; i <= end; i++) {
            copy[j++] = data[i];
        }
    }
}