package tdd;

import java.util.Arrays;
import java.util.Scanner;

public class Exercises {

	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}
		return num;
	}
	
	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		if(day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] days = { "niedziela" , "poniedzialek" , "wtorek" , "sroda" , "czwartek" , "piatek" , "sobota" };
		return days[day - 1];
	}
	
	public String determineMonthOfYear(int month) throws IllegalArgumentException {
		if(month > 12 || month < 1) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] months = { "styczen" , "luty" , "nmarzec" , "kwiecien" , "maj" , "czerwiec" , "lipiec" , "sierpien" , "wrzesien" , "pazdziernik" , "listopad" , "grudzien" };
		return months[month-1];
	}
	
	public int theBiggestCommonDivider(int first, int second) throws IllegalArgumentException {
		if(first == 0 || second == 0) {
			throw new IllegalArgumentException("Bad input value");
			}
		int pom;
		int divider = 1;
		if(first <= second) {
			pom = first;
			first = second;
			second = pom;
		}
		while(first % second != 0){
		divider = first%second;
		first = second;
		second = divider;		
		} 
		return absoluteValue(divider);
	}
	
	public int TimeInSeconds(int hours, int minutes, int seconds) throws IllegalArgumentException {		
		if(hours < 0 || minutes < 0 || seconds < 0){
			throw new IllegalArgumentException("Bad input value");
		}
			return (hours*3600)+(minutes*60)+seconds;
	}
	
	public int[] processnumbers(int a, int b) throws IllegalArgumentException {
		if(a < 0 || a > 255 || b < 0 || b > 255) {
			throw new IllegalArgumentException("Bad input value");
		}
		//Scanner s = new Scanner(System.in);
		//a = s.nextInt();
		//b = s.nextInt();
		a = 3;
		b = 8;
		int[] ret = new int[1];
		if(a < b) {
			int pom = a;
			a = b;
			b = pom;
		}
		int index = 0;
		for(int i = (a % 2 != 0) ? a + 1 : a; i <= b; i+=2){
			ret[index] = i;
			ret = Arrays.copyOf(ret, ret.length + 1);
			index++;
		}
		a = (a % 2 != 1) ? a + 1 : a;
		for(int j = b; j >= a ; j-=2){
			ret[index] = j;
			ret = Arrays.copyOf(ret, ret.length + 1);
			index++;
		}		
		return ret;
	}
	
	public int sumOfSquares(int sum){
		return sum;
	}
	
}
