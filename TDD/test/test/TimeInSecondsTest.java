package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;

public class TimeInSecondsTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenOutOfrangeHourIsGivenExceptionExpected() {
		int hour = -2;
		int minute = 4;
		int second = 20;
		IllegalArgumentException iae = null;

		try {
			e.TimeInSeconds(hour, minute, second);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test
	public void whenOutOfrangeMinuteIsGivenExceptionExpected() {
		int hour = 2;
		int minute = -5;
		int second = 20;
		IllegalArgumentException iae = null;

		try {
			e.TimeInSeconds(hour, minute, second);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test
	public void whenOutOfrangeSecondIsGivenExceptionExpected() {
		int hour = 2;
		int minute = 4;
		int second = -20;
		IllegalArgumentException iae = null;

		try {
			e.TimeInSeconds(hour, minute, second);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test 
	public void whenTimeInSecondsExpected(){
		int hour = 5;
		int minute = 55;
		int second = 40;
		int expected = 21340;
		assertEquals(expected, e.TimeInSeconds(hour, minute, second));		
	}
	
	@Test
	public void whenAllValuesAreZerosAreGivenZero() {
		int hour = 0;
		int minute = 0;
		int second = 0;
		int expected = 0;
		assertEquals(expected, e.TimeInSeconds(hour, minute, second));
	}
	
	@Test
	public void whenHourIsZerosAreGivenPositiveResultExpected() {
		int hour = 0;
		int minute = 55;
		int second = 40;
		int expected = 3340;
		assertEquals(expected, e.TimeInSeconds(hour, minute, second));
	}
	
	@Test
	public void whenMinuteIsZerosAreGivenPositiveResultExpected() {
		int hour = 5;
		int minute = 0;
		int second = 40;
		int expected = 18040;
		assertEquals(expected, e.TimeInSeconds(hour, minute, second));
	}
	
	@Test
	public void whenSecondIsZerosAreGivenPositiveResultExpected() {
		int hour = 5;
		int minute = 55;
		int second = 0;
		int expected = 21300;
		assertEquals(expected, e.TimeInSeconds(hour, minute, second));
	}
	
	

}
