package test;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;

public class ProcessNumbersTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenArgumentAIsOutOfRangeExceptionExpected() {
		IllegalArgumentException iae = null;
		int[] arrayOfA = { -1, 256, 1000, -1000 };
		int b = 8;
		for (int a : arrayOfA) {
			try {
				e.processnumbers(a, b);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertTrue(iae != null);
			iae = null;
		}
	}

	@Test
	public void whenArgumentBIsOutOfRangeExceptionExpected() {
		IllegalArgumentException iae = null;
		int a = 3;
		int[] arrayOfB = { -123, 257, -1 , 10000 };
		for (int b : arrayOfB) {
			try {
				e.processnumbers(a, b);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNotNull("Wyjątek nie wystąpił :(" ,iae);
			iae = null;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentsAreInIncorrectOrderExceptionExpected() {
		int a = 8, b = 3;
		e.processnumbers(a, b);
	}

	@Test
	public void whenArgumentsAreProperExpectArrayResult() {
		int a = 3, b = 8;
		int[] expected = { 4, 6, 8, 7, 5, 3 };
		assertArrayEquals(expected, e.processnumbers(a, b));
	}

	@Test
	public void whenArgumentAreTheSameExpectOneItemArray() {
		int arg = 5;
		int[] expected = {5};
		assertArrayEquals(expected, e.processnumbers(arg, arg));
	}
}
