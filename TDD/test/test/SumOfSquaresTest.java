package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;

public class SumOfSquaresTest {

// <1,5>
// 1 + 4 + 9 + 16 + 25	
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int number = 0;
		IllegalArgumentException iae = null;
		try {
			e.sumOfSquares(number);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil" , iae);	
	}
	
	@Test
	public void whenNumberIsOne() {
		int number = 1;
		int expected = 1;
		assertEquals(1, e.sumOfSquares(number));
	}
	
	
	

}
