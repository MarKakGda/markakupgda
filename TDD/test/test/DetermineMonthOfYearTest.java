package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;

public class DetermineMonthOfYearTest {
	
	Exercises e;
	
	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenOutOfRangeValueIsGivenExceptionExpected(){
		int data = -5;
		IllegalArgumentException iae = null;
		try {
			e.determineMonthOfYear(data);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil" ,iae);
	}
	
	@Test
	public void whenProperValueIsGivenProperMonthResultExpected() {
		int[] data = {1,12};
		String[] expected = {"styczen" , "grudzien" };
		for (int i = 0; i < data.length; i++){
			assertEquals(expected[i], e.determineMonthOfYear(data[i]));
		}
	}
	
	@Test
	public void whenMediumValueIsGivenProperMonthResultExpected() {
		int data = 6;
		String expected = "czerwiec";
			assertEquals(expected, e.determineMonthOfYear(data));
	}
	

}
