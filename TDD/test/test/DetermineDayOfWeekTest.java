package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;


public class DetermineDayOfWeekTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}
	
	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int data = -14;
		IllegalArgumentException iae = null;
		
		try {
			e.determineDayOfWeek(data);
		} catch(IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentIsGivenExceptionExpected() {
		int data = 123;
		e.determineDayOfWeek(data);
	}

	@Test
	public void whenProperValueIsGivenProperDayResultExpected() {
		// testujemy wartości brzegowe
		int[] data = {1,7};
		String[] expected = { "niedziela", "sobota" };
		for(int i = 0; i < data.length; i++){
			assertEquals(expected[i], e.determineDayOfWeek(data[i]));
		}
	}
	
	@Test
	public void whenMediumValueIsGivenProperDayResultExpected() {
		int data = 4;
		String expected = "sroda";
			assertEquals(expected, e.determineDayOfWeek(data));
	}
	
	
}