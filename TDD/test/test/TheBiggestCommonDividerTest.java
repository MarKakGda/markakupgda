package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.Exercises;

public class TheBiggestCommonDividerTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenFirstIsZero(){
		int number1 = 0;
		int number2 = 1;
		IllegalArgumentException iae = null;
		try {
			e.theBiggestCommonDivider(number1, number2);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test
	public void whenSecondIsZero(){
		int number1 = 1;
		int number2 = 0;
		IllegalArgumentException iae = null;
		try {
			e.theBiggestCommonDivider(number1, number2);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil" , iae);
	}
	
	@Test
	public void whenBothArePositiveIsGivenPositiveDividerIsExpected() {
		int number1 = 174;
		int number2 = 18;
		int expected = 6;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenFirstIsSmallerThanSecond() {
		int number1 = 18;
		int number2 = 174;
		int expected = 6;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenFirstIsPositiveSecondIsNegative() {
		int number1 = -174;
		int number2 = 18;
		int expected = 6;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenFirstIsNegativeSecondIsPositive() {
		int number1 = 174;
		int number2 = -18;
		int expected = 6;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenBothAreNegativeIsGivenPositiveDividerIsExpected() {
		int number1 = -174;
		int number2 = -18;
		int expected = 6;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenBothArePrimalNumbers(){
		int number1 = 31;
		int number2 = 17;
		int expected = 1;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	@Test
	public void whenBothAreOnes(){
		int number1 = 1;
		int number2 = 1;
		int expected = 1;
		assertEquals(expected, e.theBiggestCommonDivider(number1, number2));
	}
	
	

}
