package Cities;

import Cities.connector.DatabaseConnector;
import Cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-06.
 */
public class Main {
    public static void main(String[] args) {


        // dbhost, dbport, dbname, dbuser, dbpass

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO city VALUES(4, 'Olsztyn')");
            s.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "UPDATE city SET name = ? WHERE name = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(2,"Gdynia");
            ps.setString(1,"Włocławek");
            int countRows = ps.executeUpdate();
            System.out.println("Nadpisałem " + countRows + " wiersz(y).");
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM city WHERE name = 'Sosnowiec'");
            s.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }



        // 1. Potrzebujemy połączenie
        // select
        List<City> listOfCities = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM city";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                listOfCities.add(new City(rs.getInt("id"), rs.getString("name")));
            }
            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        for(City c : listOfCities) {
            System.out.println(c.getId() + ". " + c.getName());
        }

        // Utworzenie poszczególnych przykładów obrazujących UDATE jak i DELETE
    }
}
