package Cities.model;

/**
 * Created by RENT on 2017-07-06.
 */
public class City {
    private int id;
    private String name;

    public City() {}

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public City setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public City setName(String name) {
        this.name = name;
        return this;
    }
}
