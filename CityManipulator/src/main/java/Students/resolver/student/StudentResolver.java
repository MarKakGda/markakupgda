package Students.resolver.student;

import Cities.connector.DatabaseConnector;
import Students.model.Student;
import Students.resolver.AbstractResolver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public class StudentResolver extends AbstractResolver<Student> {

    @Override
    public Student get(int id) {
        Student ret = null;
        List<Student> listOfStudents = new ArrayList<>();
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM students";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                ret = new Student(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
            }
            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        for(Student s : listOfStudents) {
            System.out.println(s.getId() + ". " + s.getFirstname() + s.getLastname());
        }
        return ret;
    }

    @Override
    public boolean delete(int id) {
        boolean ret = false;
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM student WHERE id = "+id);
            s.close();
            ret = true;
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    @Override
    public boolean update(int id, Map<String,String> params) {
        boolean ret = false;
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("UPDATE student SET lastname = ?  WHERE id = "+id);
            s.close();
            ret = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    @Override
    public boolean insert(Map<String, String> params) {
        boolean ret = false;
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO student VALUES ("+params.get("id")+", "+params.get("firstname")+", "+params.get("lastname")+")");
            s.close();
            ret = true;
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }
}
