package Students.resolver;

import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public abstract class AbstractResolver<T> {

    public abstract T get(int id);

    public abstract boolean delete(int id);

    public abstract boolean insert(Map<String, String> params);

    public abstract boolean update(int id, Map<String, String> params);
}
