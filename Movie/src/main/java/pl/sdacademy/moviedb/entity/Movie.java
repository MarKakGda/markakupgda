package pl.sdacademy.moviedb.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;
    @Column(name = "title")
    String title;
    @Column(name = "year")
    int year;
    @Column(name = "duration")
    double duration;
    @Column(name = "description")
    String description;
    @OneToMany
    @Column(name = "genre")
    int genre;

    public Movie() {
    }

    public Movie(int id, String title, int year, double duration, String description, int genre) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.description = description;
        this.genre = genre;
    }

    public Movie(String title, int year, double duration, String description, int genre) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.description = description;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public Movie setId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getYear() {
        return year;
    }

    public Movie setYear(int year) {
        this.year = year;
        return this;
    }

    public double getDuration() {
        return duration;
    }

    public Movie setDuration(double duration) {
        this.duration = duration;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Movie setDescription(String description) {
        this.description = description;
        return this;
    }
}

