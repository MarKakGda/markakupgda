package pl.sdacademy.moviedb.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */

@Entity
@Table(name = "genre")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;
    @Column(name = "name")
    String name;
}
