package main;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {

		Main.printAdresses("Algorytmiczna", 4);
	}
	
	public static void printAdresses(String street, int num){
		String cage;
		for(int block = 1; block <= num; block += 2){
		
			for(int apartment = 1; apartment <= 12; apartment++){
				if(apartment > 6){
					cage = "B";
				} else {
					cage = "A";
				}
				System.out.println("ul. " + street + " " + block + "/" + cage + "/" + apartment);
			}
	
			
		}
		
		
	}
	

}
