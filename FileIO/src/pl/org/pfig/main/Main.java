package pl.org.pfig.main;

import java.util.Arrays;
import java.util.List;

import pl.org.pfig.exercises.AvgChecker;
import pl.org.pfig.exercises.Columner;
import pl.org.pfig.exercises.LengthChecker;
import pl.org.pfig.exercises.MoneyConverter;
//import pl.org.pfig.exercises.Sentencer;
import pl.org.pfig.exercises.TempConverter;
import pl.org.pfig.exercises.WrongColumnException;

public class Main {

	public static void main(String[] args) {
		
		Columner col = new Columner("data.txt");
		try {
			System.out.println(col.minColumn(1));
		} catch (WrongColumnException e) {
			System.out.println(e.getMessage());
		}
		
		//AvgChecker avgc = new AvgChecker();
		//avgc.process();
		
		/*
		MoneyConverter mc = new MoneyConverter();
		
		List<String> currencies = Arrays.asList("USD" , "EUR" , "JPY");
		
		for(String c : currencies) {
			System.out.println(c + " | " + mc.readCourse(c));
		}
		*/
		//System.out.println(mc.convert(1, "EUR"));
		//System.out.println(mc.convert(1, "EUR" , "USD"));
		
	/*	TempConverter tc = new TempConverter();
		double[] tempC = tc.readTemp("tempC.txt");
		
		tc.writeTemp(tempC);
		
		LengthChecker lc = new LengthChecker();
		lc.make("words.txt", 8);
		*/
		
		
	/*	Sentencer s = new Sentencer();
		
		String mySentence = s.readSentence("test.txt");
		 
		s.writeSentence("mySentence.txt", mySentence);
		
		
		
		String myStr = "1 CNY	0,6064";
		String[] temp = myStr.split("\t");
		String[] values = temp[0].split(" ");
		
		int howMany = Integer.parseInt(values[0]);
		String currency = values [1];
		double course = Double.parseDouble(temp[1].replace(",", "."));
		
		System.out.println(howMany + " | " + currency + " | " + course);
	}
	*/
	}
}
