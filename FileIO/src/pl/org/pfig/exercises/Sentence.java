package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {
	
	public String readSentence(String filename){
		String sentence = "";
		File f = new File("resources/" + filename);
		
		try {
			Scanner sc = new Scanner(f);
			
			while(sc.hasNextLine()){
				sentence += sc.nextLine() + " ";
			}
			
			sc.close();
			
			sentence = sentence.toUpperCase().charAt(0) + sentence.substring(1, sentence.length() -1);
			
			if(!sentence.endsWith(".")) {
				sentence += ".";
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		
		return sentence;
	}
}
