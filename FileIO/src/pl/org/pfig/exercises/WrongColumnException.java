package pl.org.pfig.exercises;

public class WrongColumnException extends Exception {
	public WrongColumnException(String message){
		super(message);
	}

}
