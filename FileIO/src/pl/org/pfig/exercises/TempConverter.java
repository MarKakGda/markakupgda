package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {
	
	private final String path = "resources/";
	
	public double toKelvin(double c){
		return c + 273.15;
	}
	
	public double toFahrenheit(double c){
		return (c*1.8) + 32;
	}
	
	public double[] readTemp(String filename){
		double[] ret = new double[countLines(filename)];
		File f = new File(path + filename);
		
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
			int i = 0;
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				ret[i++] = Double.parseDouble(currentLine.replace(",", "."));
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	

	public int countLines(String filename){
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
			
		}
		return lines;
	}
	
	public void writeTemp(double[] temp) {
	File toK = new File(path + "tempK.txt");
	File toF = new File(path + "tempF.txt");
	
	try {
		FileOutputStream fosK = new FileOutputStream(toK);
		FileOutputStream fosF = new FileOutputStream(toF);
		
		PrintWriter pwK = new PrintWriter(fosK);
		PrintWriter pwF = new PrintWriter(fosF);
		
		for(double c : temp){
		pwK.println(toKelvin(c));
		pwF.println(toFahrenheit(c));
		}
		
		pwK.close();
		pwF.close();
		
	} catch (FileNotFoundException e) {
		System.out.println("File not found.");
	}
}

}
