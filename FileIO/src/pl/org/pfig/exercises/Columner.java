package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {

	private final String path = "resources/";
	private String filename;
	private double[] currentColumn;
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner() {
	}

	public Columner(String filename) {
		this.filename = filename;
	}

	public double sumColumn(int column) throws WrongColumnException {
		double sum = 0;
		for(double d : readColumn(column)){
			sum += d;
		}
		return sum;
		
	}

	public double avgColumn(int column) throws WrongColumnException {
		return sumColumn(column) / currentColumn.length;
	}

	public int countColumn(int column) throws WrongColumnException {
		readColumn(column);
		return currentColumn.length;
	}

	public double maxColumn(int column) throws WrongColumnException {
		double max = 0;
		for(double d : readColumn(column)){
			if(max < d){
				max = d;
			}		
		}
		return max;
	}

	public double minColumn(int column) throws WrongColumnException {
		double min = 0;
		for(double d : readColumn(column)){
			if(min == 0 || min > d){
				min = d;
			}
		}
		return min;
	}

	private void readFile() {
		if (fileContent.size() == 0) {
			File f = new File(path + filename);

			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void writeColumn(String filename) {
		File f = new File(path + filename);
		
		try {
			Scanner sc = new Scanner(f);
			
			while(sc.hasNextLine()){
				
			}
			sc.close();
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			
			
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public double[] readColumn(int column) throws WrongColumnException {
		readFile();
		int maxColNum = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColNum) {
			currentColumn = new double[fileContent.size()];
			int i = 0;
			for(String line : fileContent)  {
				String[] cols = line.split("\t");
				currentColumn[i++] = Double.parseDouble(cols[column - 1].replace(",", "."));
			}
		} else {
			throw new WrongColumnException("Bad column value");
		}
		return currentColumn;
	}

}
