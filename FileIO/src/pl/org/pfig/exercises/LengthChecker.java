package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
	
	
	private final String path = "resources/";
	
	private boolean isProperLength(String arg, int len){
		return arg.length() > len;
	}
	
	private String[] readFile(String filename){
		
		String[] ret = new String[countWords(filename)];
		File f = new File(path + filename);
	
		try {
			Scanner sc = new Scanner(f);
			String currentWord;
			int i = 0;
			while(sc.hasNextLine()) {
				ret[i++] = sc.nextLine();
				
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	
	private int countWords(String filename){
		File f = new File(path + filename);
		int words = 0;
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				words++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			
		}
		return words;
	}
	
	private void writeFile(String[] fileContent, int len){
		try {
			FileOutputStream fos = new FileOutputStream(path + "words_" + len + ".txt");
			PrintWriter pw = new PrintWriter(fos);
			for(String line : fileContent){
				if(isProperLength(line, len)){
					pw.println(line);
				}
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	public void make(String fileInput, int len){
		String[] fileContent = readFile(fileInput);
		writeFile(fileContent, len);
		
	}
}
