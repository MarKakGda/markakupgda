package ScreenRecorder;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		ScreenRecorder sr = new ScreenRecorder();
		
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			if(splits[0].equals("add")) {
				String codecType = splits[1];
				Codec codec = Codec.valueOf(codecType);
				
				String extensionType = splits[2];
				Extension extension = Extension.valueOf(extensionType);
				
				String resolutionType = splits[3];
				Resolution resolution = Resolution.valueOf(resolutionType);
				
				Profile p = new Profile(codec, extension, resolution);
				sr.addProfile(p);
			} else if (splits[0].equals("exit")){
				break;
			} else if (splits[0].equals("list")){
				sr.listProfiles();
			} else if (splits[0].equals("start")){
				sr.startRecording();
			} else if (splits[0].equals("stop")){
				sr.stopRecording();
			} else if(splits[0].equals("setProfile")){
				Integer index = Integer.parseInt(sc.nextLine());
				sr.setProfile(index);
			}
		}
	}
	
	public static Profile createProfile() {
		Scanner sc = new Scanner(System.in);
		
		String codecType = sc.nextLine();
		Codec codec = Codec.valueOf(codecType);
		
		String extensionType = sc.nextLine();
		Extension extension = Extension.valueOf(extensionType);
		
		String resolutionType = sc.nextLine();
		Resolution resolution = Resolution.valueOf(resolutionType);
		
		return new Profile(codec, extension, resolution);
	}
}
