package ScreenRecorder;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ScreenRecorder {
	
	private Profile currentProfile = null;
	private List<Profile> profiles = new LinkedList<>();
	private Boolean isRecording = false;
	
	public void addProfile(Profile p){
		profiles.add(p);
	}
	
	public void setProfile(int numberOfProfile){
		if(numberOfProfile <1 || numberOfProfile > profiles.size()){
			System.out.println("Podany profil nie istnieje");
		}else{
			currentProfile = profiles.get(numberOfProfile);
		}
	}
	
	public void listProfiles(){
		for(int i = 0; i < profiles.size(); i++){
			System.out.println((i+1) + " : " + profiles.get(i));
		}
	}
	

	public void startRecording() {
		if(currentProfile == null){
			System.out.println("Set Profile");
		}else if(isRecording){
			System.out.println("Currently recording");
		}else{
			System.out.println("Recording started: " + currentProfile);
			isRecording = true;
		}	
	}
	
	public void stopRecording() {
		if(isRecording){
			System.out.println("Recording stopped");
			isRecording = false;
		}else{
			System.out.println("Nothing to stop.");
		}
	}

}
