package ScreenRecorder;

public enum Resolution {
	R_1920x1080, R_1280x768, R_1024x768, R_600x480;
}
