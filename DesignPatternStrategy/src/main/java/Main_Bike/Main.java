package Main_Bike;

import BikeFactory.Bike;
import BikeFactory.BikeFactory;

public class Main {

	public static void main(String[] args) {
		
		BikeFactory factory = new BikeFactory();
		Bike b = factory.create1GearGoetzeBike();
		Bike b2 = factory.create3GearInianaBike();
		System.out.println(b);
		System.out.println(b2);
	}

}
