package BikeFactory;

public class BikeFactory {
	
	public static Bike create5GearKrossBike(){
		return new Bike("KROSS", 1, 5, BIKE_TYPE.bicycle);
	}
	
	public static Bike create6GearMeridaBike(){
		return new Bike("Merida", 1, 6, BIKE_TYPE.bicycle);
	}
	
	public static Bike create3GearInianaBike(){
		return new Bike("Iniana", 2, 3, BIKE_TYPE.tandem);
	}
	
	public static Bike create6GearFeltBike(){
		return new Bike("FELT", 1, 6, BIKE_TYPE.bicycle);
	}
	
	public static Bike create1GearGoetzeBike(){
		return new Bike("Goetze", 2, 1, BIKE_TYPE.tandem);
	}

}
