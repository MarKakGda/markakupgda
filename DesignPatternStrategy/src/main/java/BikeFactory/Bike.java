package BikeFactory;

public class Bike {
	
	private String mark;
	private int seats;
	private int shifters;
	private BIKE_TYPE bike_type;
	
	public Bike(String mark, int seats, int shifters, BIKE_TYPE bike_type) {
		super();
		this.mark = mark;
		this.seats = seats;
		this.shifters = shifters;
		this.bike_type = bike_type;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getShifters() {
		return shifters;
	}

	public void setShifters(int shifters) {
		this.shifters = shifters;
	}

	public BIKE_TYPE getBike_type() {
		return bike_type;
	}

	public void setBike_type(BIKE_TYPE bike_type) {
		this.bike_type = bike_type;
	}

	@Override
	public String toString() {
		return "Bike [mark=" + mark + ", seats=" + seats + ", shifters=" + shifters + ", bike_type=" + bike_type + "]";
	}
}


