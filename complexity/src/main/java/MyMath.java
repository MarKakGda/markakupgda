import java.util.Random;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMath {

    public static int sum(int a, int b) {
        return a + b;
    }

    public static long pow(int a, int b) {
        long result = 1;
        for(int i = 0; i < b; i++) {
            result *= a;
        }
        return result;
    }

    public static int find(int[] data, int value) {
        for(int i = 0; i < data.length; i++) {
            if(data[i] == value) {
                return i;
            }
        }
        return -1;
    }

    public static int findBisection(int[] data, int value) {
        int a = 0;
        int b = data.length -1;
        int s;
        while (a<b) {
            s = (a+b)/2;
            if(value <= data[s]) {
                b = s - 1;
            } else {
                a = s + 1;
            }
        }
        if(data[a] == value) {
            return a;
        }
        return -1;
    }


}
