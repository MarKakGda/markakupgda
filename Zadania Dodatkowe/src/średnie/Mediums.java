package �rednie;

import java.util.Scanner;

public class Mediums {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Ile razy w tablicy wyst�puje warto�� : ");
		int x = s.nextInt();
		int counter = 0;
		int[] tablica = { 3, 2, 5, 1, 3, 6, 7, 3 };
		System.out.print("Oto warto�ci tablicy : [");
		for (int i = 0; i < tablica.length; i++) {
			if (tablica[i] == x) {
				counter++;
			}
			if (i != tablica.length - 1) {
				System.out.print(tablica[i] + ",");
			} else {
				System.out.print(tablica[i] + "]");
			}
		}
		System.out.println();
		System.out.println("Liczba " + x + " wyst�puje " + counter + " razy!");

		Scanner t = new Scanner(System.in);
		System.out.println("Ile razy w tablicy wyst�puje litera : ");
		char y = t.nextLine().charAt(0);
		int counter1 = 0;
		char[] tablica1 = "To jest biedronka".toCharArray();
		System.out.print("Oto zdanie : ");
		for (char c : tablica1) {
			System.out.print(c);
			if (c == y) {
				counter1++;
			}
		}
		System.out.println();
		System.out.println("Litera " + y + " wyst�puje " + counter1 + " razy!");

		Scanner u = new Scanner(System.in);
		System.out.println("Podaj 1  liczb� : ");
		int a = u.nextInt();
		System.out.println("Podaj 2  liczb� : ");
		int b = u.nextInt();
		int wynik = 1;
		int i = 0;
		while (wynik < b && a > 1) {
			System.out.println("Liczba " + a + " do pot�gi " + i + " wynosi : " + wynik);
			wynik *= a;
			i++;
			if (wynik == 1) {
				wynik++;
			}
		}
		if (a == 0) {
			System.out.println("Poda�e� liczb� 0, kt�rej nie mo�na pot�gowa�.");
		} else if (a == 1)
			System.out.println("Poda�e� liczb� 1, kt�rej kolejne pot�gi zawsze wynosz� 1.");
	}
}