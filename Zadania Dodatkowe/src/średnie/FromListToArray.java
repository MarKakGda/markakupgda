package �rednie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FromListToArray {

	public static void main(String[] args) {

		String[] a = new String[] { "Kaczka", "Pies", "Kot", "Mysz" };
		List list1 = Arrays.asList(a);
		System.out.println("Drukowanie listy : " + list1);
		
		List<Integer> list = toList(1, 2, 3, 5);
		List<String> strings = toList("A", "B", "C", "D");

		Integer[] array = new Integer[list.size()];

		fromListToArray(list, array);
		        for (Integer number : array) {
		            System.out.print(number +" ");
		        }
	}
	     

	public static <T> List<T> toList(T... numbers) {
		List<T> list = new ArrayList<T>();

		for (T number : numbers) {
			list.add(number);
		}
		return list;
	}

	public static <T> void fromListToArray(List<T> list, T[] array) {
		if (list.size() != array.length) {
			throw new IllegalArgumentException("Rozmiary s� r�ne");
		}
		for (int i = 0; i < array.length; i++) {
			array[i] = list.get(i);
		}
	}

}
