package podstawy;

import java.util.Scanner;

public class Basics {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Podaj 1 liczb� : ");
		int a = s.nextInt();
		System.out.println("Podaj 2 liczb� : ");
		int b = s.nextInt();
		System.out.println("Podaj 3 liczb� : ");
		int c = s.nextInt();
		System.out.println("Podaj ile razy ma si� wy�wietli� Hello World : ");
		int d = s.nextInt();
		Scanner t = new Scanner(System.in);
		System.out.println("Podaj imi� :");
		String z = t.nextLine();
		System.out.println("Podaj nazwisko :");
		String y = t.nextLine();
		System.out.println("Liczba < " + a + " >");
		if (a > b) {
			System.out.println("Liczba " + a + " jest wi�ksza od liczby " + b);
		} else if (b > a) {
			System.out.println("Liczba " + b + " jest wi�ksza od liczby " + a);
		} else {
			System.out.println("Obie liczby s� r�wne");
		}
		System.out.println("Twoje imi� to : " + z);
		System.out.println("Suma liczb <" + a + "," + b + "," + c + "> wynosi : " + (a + b + c));
		System.out.println("Wynik dzielenia " + a + " przez " + b + " wynosi : " + ((double) a / b));
		System.out.println("Reszta z dzielenia 9/4 wynosi : " + 9 % 4);
		if (a < b) {
			int pom = a;
			a = b;
			b = pom;
		}
		if (a % b == 0) {
			System.out.println("Liczba " + a + " jest podzielna przez " + b + ", a wynik dzielenia to : " + a / b);
		} else {
			System.out.println("Liczba " + a + " nie jest podzielna przez " + b);
		}
		System.out.println("Hello world!");
		System.out.println("Napis ma by� wydrukowany "+d+" razy!");
		for (int i = 0; i < d; i++) {
			System.out.println("Hello world!");			
		}
		System.out.println("Witaj <"+z+"><"+y+">!");
		int[] tablica = {3,2,5,1,3,6,7,3};
		System.out.print("Oto warto�ci tablicy : [");
		for(int i = 0; i<tablica.length;i++){
			if(i != tablica.length-1){
			System.out.print(tablica[i]+",");
			} else {
				System.out.print(tablica[i]+"]");
			}
		}
	}
}
