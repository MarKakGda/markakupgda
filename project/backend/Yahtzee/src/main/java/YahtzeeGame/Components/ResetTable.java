package YahtzeeGame.Components;

public interface ResetTable
{
    // Reset the state of the object
    void reset();
}