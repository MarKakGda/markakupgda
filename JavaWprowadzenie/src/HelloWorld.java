
public class HelloWorld {

	public static void main(String[] args) {
		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 15000000;
		float f = 5;
		double d = 3;
		char c = 'j';
		boolean bool = true;
		System.out.println("byte = "+b);
		System.out.println("short = "+s);
		System.out.println("int = "+i);
		System.out.println("long = "+l);
		System.out.println("float = "+f);
		System.out.println("double = "+d);
		System.out.println("char = "+c);
		System.out.println("boolean = "+bool);
		
	}

}
