
public class MyMath {
	
	public static void main(String[] args) {
		System.out.println("Minimalna to : " + min(5,10));
		System.out.println("Minimalna to : " + min(5.0,10.0));
		System.out.println("Maksymalna to : " + max(5,10));
		System.out.println("Maksymalna to : " + max(5.0,10.0));
		System.out.println("Bezwzgledna rowna : " + abs(-5));
		System.out.println("Bezwzgledna rowna : " + abs(5.0));
		System.out.println("a do potegi b daje : " + pow(2.0,3));
		System.out.println("a do potegi b daje : " + pow(3,2));
		
		
	}

	
	public static int max(int a, int b){
		if (a>b){
			return a;
		} else {
			return b;
		}
	}
	
	public static double max(double a, double b){
		if (a>b){
			return a;
		} else {
			return b;
		}
	}
	
	public static int min(int a, int b){
		if (a<b){
			return a;
		} else {
			return b;
		}
	}
	
	public static double min(double a, double b){
		if (a<b){
			return a;
		} else {
			return b;
		}
	}
	
	public static int abs(int a){
		return Math.abs(a);
	}
	
	public static double abs(double a){
		return Math.abs(a);
	}
	
	
	public static double pow(double a, int b){
		double wynik = 1;
		for (int i=0; i<b; i++){
		wynik *=a;
	}	
	return wynik;
	}
	
	public static int pow(int a, int b){
			int wynik = 1;
			for (int i=0; i<b; i++){
			wynik *=a;
		}	
		return wynik;
		}

}
