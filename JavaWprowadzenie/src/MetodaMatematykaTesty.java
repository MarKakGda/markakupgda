import org.junit.Test;

public class MetodaMatematykaTesty {

	@Test
	public void testIloczynu() {
		
		assert MetodyMatematyka.iloczyn(1, 1) == 1;
		assert MetodyMatematyka.iloczyn(0, 10) == 0;
		assert MetodyMatematyka.iloczyn(1, 10) == 10;
		assert MetodyMatematyka.iloczyn(3, 4) == 12;
		
	
	}
	
	@Test
	public void testIMniejsza() {
		
		assert MetodyMatematyka.mniejsza(1, 2) == 1;
		assert MetodyMatematyka.mniejsza(0, 10) == 0;
		assert MetodyMatematyka.mniejsza(1, 10) == 1;
		assert MetodyMatematyka.mniejsza(3, 4) == 3;
		
	
	}
	
	@Test
	public void testSzukaj(){
		
		int[] liczby = {1,2,3,5,3};
		assert MetodyMatematyka.szukaj(liczby, 3) == 2;
		assert MetodyMatematyka.szukaj(liczby, 4) == -1;
		assert MetodyMatematyka.szukaj(liczby, 5) == 3;
	}
	
	@Test
	public void testNajmniejsza(){
		
		assert MetodyMatematyka.najmniejsza(3, 4, 5) == 3;
		assert MetodyMatematyka.najmniejsza(6, 4, 5) == 4;
		assert MetodyMatematyka.najmniejsza(6, 7, 5) == 5;
	}

}
