import org.junit.Test;

public class GeoMathTesty {

	@Test
	public void squareTest(){
		assert GeoMath.squareArea(2.0) == 4.0;
		assert GeoMath.squareArea(1.5) == 2.25;
		assert GeoMath.squareArea(0) == 0;
		
	}
	
	@Test
	public void cubeTest(){
		assert GeoMath.cubeArea(2.0) == 24.0;
		assert GeoMath.cubeArea(3.0) == 54.0;
		assert GeoMath.cubeArea(4.0) == 96.0;
		
	}
	
	@Test
	public void circleTest(){
		assert GeoMath.circleArea(2.0) > 4.0*3.1415;
		assert GeoMath.circleArea(2.0) < 4.0*3.1416;
		assert GeoMath.circleArea(3.0) > 9.0*3.1415;
		assert GeoMath.circleArea(3.0) < 9.0*3.1416;
		
	}
	
	@Test
	public void cylinderTest(){
		assert GeoMath.cylinderVolume(2.0,5.0) > 20.0*3.1415;
		assert GeoMath.cylinderVolume(2.0,5.0) < 20.0*3.1416;
		assert GeoMath.cylinderVolume(3.0,5.0) > 45.0*3.1415;
		assert GeoMath.cylinderVolume(3.0,5.0) < 45.0*3.1416;
		
	}
	
	@Test
	public void coneTest(){
		assert GeoMath.coneVolume(6.0,5.0) > 60.0*3.1415;
		assert GeoMath.coneVolume(6.0,5.0) < 60.0*3.1416;
		assert GeoMath.coneVolume(3.0,5.0) > 15.0*3.1415;
		assert GeoMath.coneVolume(3.0,5.0) < 15.0*3.1416;
		
	}
	
	@Test
	public void cube2Test(){
		assert GeoMath.cubeVolume(2.0) == 8.0;
		assert GeoMath.cubeVolume(3.0) == 27.0;
		assert GeoMath.cubeVolume(4.0) == 64.0;
		
	}
	
	@Test
	public void pyramidTest(){
		assert GeoMath.pyramidVolume(2.0,3.0) == 4.0;
		assert GeoMath.pyramidVolume(3.0,4.0) == 12.0;
		assert GeoMath.pyramidVolume(1.0,6.0) == 2.0;
		
	}
	
}
