import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void absIntegerTest() {
		assert MyMath.abs(4) == 4;
		assert MyMath.abs(-4) == 4;
		assert MyMath.abs(0) == 0;
	}
	
	@Test
	public void absDoubleTest() {
		assert MyMath.abs(4.0) == 4;
		assert MyMath.abs(-4.0) == 4;
		assert MyMath.abs(0.0) == 0;
	}
	
	@Test
	public void powDoubleTest() {
		assert MyMath.pow(2.0, 3) == 8.0;
		assert MyMath.pow(3.0, 3) == 27.0;
		assert MyMath.pow(4.0, 3) == 64.0;
	}
	
	@Test
	public void powIntegerTest() {
		assert MyMath.pow(2, 3) == 8;
		assert MyMath.pow(3, 3) == 27;
		assert MyMath.pow(4, 3) == 64;
	}

}
