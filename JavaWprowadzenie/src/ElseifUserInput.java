import java.util.Scanner;

public class ElseifUserInput {

	public static void main(String[] args) {
		
		Scanner consoleScanner = new Scanner (System.in);
		
		System.out.println("Podaj liczb� : ");
		
		int x = consoleScanner.nextInt();
		
		if(x%3 == 0){
			System.out.println("Liczba "+x+" jest podzielna przez 3");
		} else if (x%2 == 0) {
			System.out.println("Liczba "+x+" nie jest podzielna przez 3 i jest parzysta");
		} else {
			System.out.println("Liczba "+x+" nie jest podzielna przez 3 i nie jest parzysta");
		}
		consoleScanner.close();
	}

}
