
public class Przeciazanie {
	
	public static void main(String[] args){
		show(4);
		show(4.0);
		show("4");
		Object o = "4";
		show(o);
		
		}
	
	public static void show(int number){
		System.out.println(" int : " + number );
	}
	
	public static void show(double number){
		System.out.println(" double : " + number );
	}
	
	public static void show(String text){
		System.out.println(" String : " + text );
		
	}
	
	public static void show(Object object){
		System.out.println(" Object : " + object );
	}
}
