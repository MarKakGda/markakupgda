
public class RysowanieMetodami {

	public static void main(String[] args) {
		rysujProstokat(3, 5);
		rysujProstokat(6, 3);
		rysujProstokat(10, 2);
		rysujProstokat(3, 2);
		rysujTrojkat(5);
	}

	private static void rysujProstokat(int w, int h){
		for(int i =1; i<=h; i++){
			rysujLinie(w);
		}
	}
	
	private static void rysujLinie(int dlugoscLinii){
		for(int j = 1; j <= dlugoscLinii; j++){
			System.out.print("*");
		}
		System.out.println();
		}
	
	private static void rysujTrojkat(int a){
		for(int i = 1; i<=a; i++){
			rysujLinie(i);
		}
	}
}
