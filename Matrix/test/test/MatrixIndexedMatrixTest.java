package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class MatrixIndexedMatrixTest {

	Matrix m;

	@Before
	public void test() {
		m = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenIndexedyMatrixExpected() {
		int[][] actual = new int[3][2];
		int[][] expected = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}

	@Test
	public void whenEmptyArrayGivenIndexedMatrixByDimensions() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		actual = m.indexedMatrix(actual);

		assertTrue(actual.length == expected.length);

		for (int i = 0; i < actual.length; i++) {
			if (actual.length != actual[i].length) {
				assertTrue(actual[i].length == expected[i].length);
			}
		}
	}

	public void whenEmptyArrayGivenIndexedyMatrixByValues() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };		
		m.indexedMatrix(actual);

		for (int i = 0; i < actual.length; i++) {
			for (int j = 0; j < actual[i].length; j++) {
				assertEquals(expected[i][j], actual[i][j]);
			}
		}
	}

}
