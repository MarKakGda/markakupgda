package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class MatrixDimensionMatrixTest {
	
	Matrix m;

	@Before
	public void test() {
		m = new Matrix();
	}
	
	@Test
	public void whenEmptyArrayGivenIndexedMatrixByDimensions() {
		int[][] actual = new int[3][3];
		int[][] expected = new int[3][3];
		actual = m.indexedMatrix(actual);

		assertTrue(actual.length == expected.length);

		for (int i = 0; i < actual.length; i++) {
			if (actual.length != actual[i].length) {
				assertTrue(actual[i].length == expected[i].length);
			}
		}
	}

}
