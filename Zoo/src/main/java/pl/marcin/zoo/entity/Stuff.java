package pl.marcin.zoo.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-12.
 */
@Entity
@Table(name="stuff")
public class Stuff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;
    @Column(name="lastname")
    private String lastname;
    @Column(name="age")
    private int age;

    public Stuff() {
    }

    public Stuff(int id, String name, String lastname, int age) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public Stuff(String name, String lastname, int age) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public Stuff setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Stuff setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Stuff setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Stuff setAge(int age) {
        this.age = age;
        return this;
    }
}
