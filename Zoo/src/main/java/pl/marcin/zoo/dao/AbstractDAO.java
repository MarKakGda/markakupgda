package pl.marcin.zoo.dao;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public interface AbstractDAO<T> {
    boolean insert(T type);
    boolean delete(T type);
    boolean delete(int id);
    boolean update(T type);
    T get(int id);
    List<T> get();
}
