package pl.marcin.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.marcin.zoo.entity.Stuff;
import pl.marcin.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class StuffDAO implements AbstractDAO<Stuff> {

    public boolean insert(Stuff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();
        return true;
    }

    public boolean delete(Stuff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
        return true;
    }

    public boolean update(Stuff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();
        return true;
    }

    public Stuff get(int id) {
        Stuff stuff;
        Session session = HibernateUtil.openSession();
        stuff = session.load(Stuff.class, id);
        session.close();
        return stuff;
    }

    public List<Stuff> get() {
        List<Stuff> stuffs;
        Session session = HibernateUtil.openSession();
        stuffs = session.createQuery("FROM Stuff").list();
        session.close();
        return stuffs;
    }
}
