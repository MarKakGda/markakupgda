package pl.marcin.zoo;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.marcin.zoo.dao.AnimalDAO;
import pl.marcin.zoo.dao.StuffDAO;
import pl.marcin.zoo.entity.Animal;
import pl.marcin.zoo.entity.Stuff;
import pl.marcin.zoo.util.HibernateUtil;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-12.
 */
public class Main {
    public static void main(String[] args) {

        AnimalDAO animalDAO = new AnimalDAO();
        StuffDAO stuffDAO = new StuffDAO();

        Animal a = new Animal();
        a.setName("Słoń");

        animalDAO.insert(a);
        animalDAO.insert(new Animal("Orzeł"));
        animalDAO.insert(new Animal("Gołąb"));

        Stuff s  = new Stuff();
        s.setName("Jan").setLastname("Kowalski").setAge(25);

        stuffDAO.insert(s);
        stuffDAO.insert(new Stuff("Marcin","Nowak",23));
        stuffDAO.insert(new Stuff("Janusz","Januszowy",22));

        Animal slon = animalDAO.get(1);

        System.out.println("Pobrales id = " + slon.getId() + " " + slon.getName());

        slon.setName("Przerobiony słoń");
        animalDAO.update(slon);

        animalDAO.delete(3); // delete gołąb

        Animal secondAnimal = new Animal();
        secondAnimal.setId(2);
        animalDAO.delete(secondAnimal); // delete orzeł

        AnimalDAO animaldao = new AnimalDAO();
        for(Animal anim : animaldao.get()) {
            System.out.println(anim.getId() +". "+anim.getName());
        }
    }
}