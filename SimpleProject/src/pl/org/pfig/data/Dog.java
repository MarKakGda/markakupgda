package pl.org.pfig.data;

public class Dog implements AnimalInterface{
	private String name;

	public Dog(String name) {
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	public Dog newDog(String name) {
		return new Dog(name);
	}
	@Override
	public String toString() {
		return "Dog: "+this.name;
	}
}
