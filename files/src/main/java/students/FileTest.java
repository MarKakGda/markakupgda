package students;

import java.util.ArrayList;
import java.util.List;

public class FileTest {
	
	public static void main(String[] args) {
		
		List<Students> students = new ArrayList<>();
		students.add(new Students(100, "Jan", "Kowalski"));
		students.add(new Students(101, "Jan1", "Kowalski1"));
		University u = new University(students);
		
		IFile textFile = new XMLFile();
		
		textFile.save(u.getStudentsList());
		
		List<Students> studentsFromFile = textFile.load();
		University universityFromFile = new University(studentsFromFile);
		
		universityFromFile.showAll();
	}

}
