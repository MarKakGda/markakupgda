package students;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	private Map<Integer, Students> students = new HashMap<>();

	public University() {
	}

	public University(List<Students> studentsList) {
		for (Students student : studentsList) {
			students.put(student.getIndexnumber(), student);
		}
	}

	public void addStudent(int indexnumber, String name, String surname) {
		Students student = new Students(indexnumber, name, surname);
		students.put(student.getIndexnumber(), student);
	}

	public boolean studentExists(int indexnumber) {
		return students.containsKey(indexnumber);
	}

	public Students getStudents(int indexnumber) {
		return students.get(indexnumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Students s : students.values()) {
			System.out.println(s.getIndexnumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}

	public List<Students> getStudentsList() {
		List<Students> studentsList = new LinkedList<>();

		for (Students s : students.values()) {
			studentsList.add(s);
		}
		return studentsList;
	}

	public static void main(String[] args) {
		List<Students> students = new ArrayList<>();
		students.add(new Students (100, "Jan", "Kowalski"));
		students.add(new Students (101, "Adam", "Kowalski"));
		students.add(new Students (102, "Jan", "Nowak"));
		students.add(new Students (103, "Adam", "Nowak"));
		University u = new University(students);
		u.showAll();
		
		for (Students student : u.getStudentsList()) {
		System.out.println(student);
		}
		
	}

}
