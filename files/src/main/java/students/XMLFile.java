package students;

import static xml.ParseXml.getText;
import static xml.ParseXml.parseFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseStudentsXml;
import xml.ParseXml;

public class XMLFile implements IFile {

	@Override
	public void save(List<Students> studentsList) {
		
	try {
		Document doc = ParseXml.newDocument();
		createContent(doc, studentsList);
		ParseXml.saveFile(doc, "students.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent(Document doc, List<Students> studentsList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);
		for(Students student : studentsList){
			addStudent(doc, students, student);
		}
	}

	public void addStudent(Document doc, Element students, Students student) {
		Element studentElement = doc.createElement("student");
		studentElement.setAttribute("index", String.valueOf(student.getIndexnumber()));
		students.appendChild(studentElement);
		
		Element studentName = doc.createElement("name");
		studentName.appendChild(doc.createTextNode(student.getName()));
		studentElement.appendChild(studentName);
		
		Element studentSurname = doc.createElement("surname");
		studentSurname.appendChild(doc.createTextNode(student.getSurname()));
		studentElement.appendChild(studentSurname);
	}
	
	

	@Override
	public List<Students> load() {
		List<Students> studentsList = new ArrayList<>();
		try {
			Document doc = ParseXml.parseFile("students.xml");
			NodeList students = doc.getElementsByTagName("student");
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Students s = parse(studentNode);
				studentsList.add(s);
				
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return studentsList;
	
	}

	private Students parse(Node studentNode){
		System.out.println("\nCurrent Element : " + studentNode.getNodeName());
		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) studentNode;
			System.out.println("Index number : " + student.getAttribute("index"));
			System.out.println("Name : " + ParseXml.getText(student, "name"));
			System.out.println("Surname : " + ParseXml.getText(student, "surname"));
		}
		return null;
	}
}
