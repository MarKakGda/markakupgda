package students;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements IFile {
	
	private final static String FILE_NAME = "students_text.txt";

	/* (non-Javadoc)
	 * @see students.IFile#save(java.util.List)
	 */
	@Override
	public void save(List<Students> studentsList) {
		try (PrintStream printStream = new PrintStream(FILE_NAME)){
			for(Students student : studentsList){
				printStream.println(student.getIndexnumber() + " " + student.getName() + " " + student.getSurname());
			}
			printStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see students.IFile#load()
	 */
	@Override
	public List<Students> load() {
		
		List<Students> students = new ArrayList<>();
		
		try (Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(FILE_NAME)))){
			while(scanner.hasNextLine()){
				String currentLine = scanner.nextLine();
				String[] temp = currentLine.split(" ");
				Students student = new Students(Integer.parseInt(temp[0]), temp[1], temp[2]);
				students.add(student);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return students;
	}

}
