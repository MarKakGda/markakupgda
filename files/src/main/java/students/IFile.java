package students;

import java.util.List;

public interface IFile {

	void save(List<Students> studentsList);

	List<Students> load();

}