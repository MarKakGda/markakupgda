package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {

	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Students> studentsList) {

		DataOutputStream out = null;
		try {
			FileOutputStream fout = new FileOutputStream(FILE_NAME);
			BufferedOutputStream bouf = new BufferedOutputStream(fout);
			out = new DataOutputStream(bouf);

			for (Students student : studentsList) {
				out.writeInt(student.getIndexnumber());
				out.writeUTF(student.getName());
				out.writeUTF(student.getSurname());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public List<Students> load() {

		List<Students> students = new ArrayList<>();
		try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))) {

			while (in.available() > 0) {
				int indexNumber = in.readInt();
				String name = in.readUTF();
				String surname = in.readUTF();
				Students student = new Students(indexNumber, name, surname);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
		return students;
	}
}
