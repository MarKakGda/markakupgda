package animals;

public class AnimalPlanet {
	public static void main(String[] args) {
		Animals dog = new Dog("Fluffy", "mammals" , 2);
		Animals frog = new Frog("Jumpy" , "amphibians" , 2);
		Animals snake = new Snake("Creeper" , "reptiles" , 2);
		Animals pidgeon = new Pidgeon ("Wingy" , "birds" , 2);
		
		Animals[] world = {dog, frog, snake, pidgeon};
		
		for(Animals animals : world){
			animals.introduce();
			animals.yourFamily();
			animals.yourEyes();
			System.out.println("");
		}
	}

}
