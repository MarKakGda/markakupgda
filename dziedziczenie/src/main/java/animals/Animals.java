package animals;

public abstract class Animals {
	
	private String name;
	private String type;
	private int eyes;
	
	public Animals(String name, String type, int eyes){
		this.name = name;
		this.type = type;
		this.eyes = eyes;
	}
	
	public String getName(){
		return name;
	}
	
	public String getType(){
		return type;
	}
	
	public int getEyes(){
		return eyes;
	}
	
	public abstract void introduce();
	public abstract void yourFamily();
	public abstract void yourEyes();

}
