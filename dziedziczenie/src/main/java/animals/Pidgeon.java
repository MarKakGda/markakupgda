package animals;

public class Pidgeon extends Animals {

	public Pidgeon (String name, String type, int eyes){
		super(name, type, eyes);
	}
	
	public void introduce(){
		System.out.println("This is pidgeon and it's called "+this.getName()+".");
	}
	
	public void yourFamily(){
		System.out.println("It belongs to family called "+this.getType()+".");
	}
	public void yourEyes(){
		System.out.println("It's got "+this.getEyes()+" eyes.");
	}
}
