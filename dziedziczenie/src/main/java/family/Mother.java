package family;

public class Mother {
	
	private String name;
	
	public Mother(){
	}
	
	public Mother (String name){
		this.name = name;
	}
	
	public void setName (String name){
		this.name = name;
	}
	
	public String getName (){
		return this.name;
	}
}
