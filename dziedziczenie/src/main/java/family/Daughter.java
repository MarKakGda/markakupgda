package family;

public class Daughter {

private String name;
	
	public Daughter(){
	}
	
	public Daughter(String name){
		this.name = name;
	}
	
	public void setName (String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}

