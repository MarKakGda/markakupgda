package family.dziedziczenie;

public class Family {
	
	public static void main(String[] args) {
		FamilyMember mother = new Mother("Gra�yna");
		FamilyMember father = new Father("Janusz");
		FamilyMember son = new Son("Tomasz");
		FamilyMember daughter = new Daughter("Ania");
		
		FamilyMember[] members = {mother, father, son, daughter};
		
		for(FamilyMember familymember : members){
			familymember.introduce();
		}
	}
}
