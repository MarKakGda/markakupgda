package wzorymatematyczne;

public class Square implements Math{
	
    private double a = 5.0;
    
    public Square(){
    }

    public double area(){
         return a * a;
    }
    
    public double circumference(){
    	return 4*a;
    }
}