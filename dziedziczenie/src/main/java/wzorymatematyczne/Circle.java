package wzorymatematyczne;

public class Circle implements Math{
	
	private double r = 3.0;
	private double PI = 3.14;
	
	public Circle(){
    }

    public double area(){
    	return PI*r*r;
    }
    
    public double circumference(){
    	return 2*PI*r;
    }
}
