package pl.marcin.repository;

import org.springframework.data.repository.CrudRepository;
import pl.marcin.entity.Profession;

/**
 * Created by RENT on 2017-07-27.
 */
public interface ProfessionRepository extends CrudRepository<Profession, Long> {
}
