package pl.marcin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marcin.entity.Profession;
import pl.marcin.repository.ProfessionRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */

@CrossOrigin
@RestController
@RequestMapping("/professions")
public class ProfessionController {
    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/add")
    public Profession add(@RequestParam(name="name") String name) {
        Profession p = new Profession();
        p.setName(name);
        return professionRepository.save(p);
    }

    @RequestMapping("/show")
    public List<Profession> showAll() {
        return (List<Profession>) professionRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Profession showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return professionRepository.findOne(myId);
    }
}
