package pl.marcin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marcin.entity.Specie;
import pl.marcin.repository.SpecieRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/species")
public class SpecieController {

    @Autowired
    private SpecieRepository specieRepository;

    @RequestMapping("/add")
    public Specie add(@RequestParam(name="name") String name,
                      @RequestParam(value = "description") String desc) {
        Specie s = new Specie();
        s.setName(name);
        s.setDescription(desc);
        return specieRepository.save(s);
    }

    @RequestMapping("/show")
    public List<Specie> showAll() {
        return (List<Specie>) specieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Specie showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return specieRepository.findOne(myId);
    }
}
