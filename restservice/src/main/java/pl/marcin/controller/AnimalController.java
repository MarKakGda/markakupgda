package pl.marcin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marcin.entity.Animal;
import pl.marcin.entity.Specie;
import pl.marcin.repository.AnimalRepository;
import pl.marcin.repository.SpecieRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@CrossOrigin
@RestController
@RequestMapping("/animals")
public class AnimalController {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private SpecieRepository specieRepository;

    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Animal> listAnimals() {
        return (List<Animal>) animalRepository.findAll();
    }

    @RequestMapping("/add")
    public Animal addAnimal(@RequestParam(value = "name") String name,
                            @RequestParam(value = "description") String desc,
                            @RequestParam(value = "image") String image,
                            @RequestParam(value = "specie") String specie) {
        long specieId = Long.valueOf(specie);
        Specie s = specieRepository.findOne(specieId);
        Animal a = new Animal();
        a.setName(name);
        a.setDescription(desc);
        a.setImage(image);
        a.setSpecie(s);
        return animalRepository.save(a);
    }

    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id) {
        return animalRepository.findOne(Long.valueOf(id));
    }

    @RequestMapping("/delete/{id}")
    public boolean deleteAnimal(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        animalRepository.delete(animalRepository.findOne(myId));
        return true;
    }

    @RequestMapping("/edit")
    public Animal editAnimal(@RequestParam(name = "id") String id,
                             @RequestParam(name = "name") String name,
                             @RequestParam(name = "description") String desc,
                             @RequestParam(name = "image") String image,
                             @RequestParam(name = "specie") String specie) {
        long myId = Long.valueOf(id);
        long specieId = Long.valueOf(specie);
        Specie s = specieRepository.findOne(specieId);
        Animal a = new Animal();
        a.setId(myId);
        a.setName(name);
        a.setDescription(desc);
        a.setImage(image);
        a.setSpecie(s);
        return animalRepository.save(a);
    }
}