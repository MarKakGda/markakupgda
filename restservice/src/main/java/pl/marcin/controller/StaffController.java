package pl.marcin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marcin.entity.Profession;
import pl.marcin.entity.Staff;
import pl.marcin.repository.ProfessionRepository;
import pl.marcin.repository.StaffRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */

@CrossOrigin
@RestController
@RequestMapping("/staffs")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/")
    public String staff() {
        return "";
    }

    @RequestMapping("/show")
    public List<Staff> listStaffs() {
        return (List<Staff>) staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addStaff  (@RequestParam(value = "name") String name,
                            @RequestParam(value = "lastname") String lastname,
                            @RequestParam(value = "salary") String salary,
                            @RequestParam(value = "profession") String profession) {
        long professionId = Long.valueOf(profession);
        Profession p = professionRepository.findOne(professionId);
        Staff s = new Staff();
        s.setName(name);
        s.setLastname(lastname);
        s.setSalary(Double.valueOf(salary));
        s.setProfession(p);
        return staffRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public Staff showStaffByID(@PathVariable("id") String id) {
        return staffRepository.findOne(Long.valueOf(id));
    }
}
