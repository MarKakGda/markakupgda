package UserGenerator;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;


public class UserGenerator {

	private final String path = "resources/";

	public User getRandomUser() {
		UserSex us = UserSex.SEX_MALE;
		if(new Random().nextInt(2)==0) {
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	}

	public User getRandomUser(UserSex sex) {
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if(sex.equals(UserSex.SEX_MALE)) {
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}		
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setAddress(getRandomAddress());
		String dt = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		currentUser.setPhone(getRandomPhoneNumber());
		currentUser.setCCN(getRandomCCN());
		currentUser.setPESEL(getRandomPESEL(currentUser));
		
		return currentUser;
	}

	private Address getRandomAddress() {
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("ul. "+getRandomLineFromFile("street.txt"));
		adr.setNumber("" + (new Random().nextInt(100) + 1));
		return adr;
	}
	
	private String getRandomBirthDate(){
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
			daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1] + 1);
		return leadZero(day)+"."+leadZero(month)+"."+year;
	}
	
	private String leadZero(int arg){
		if(arg < 10) return "0" + arg;
		else return "" + arg;
	}
	
	private String getRandomPESEL(User data){
		SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy");
		String[] dropped = sdf.format(data.getBirthDate()).split("");
		String day = dropped[0] + dropped[1], month = dropped[3] + dropped[4], year = dropped[8] + dropped[9];
		String pesel = "";
		for(int i = 0; i < 5; i++) {
			int num = new Random().nextInt(10);
			pesel += num;
			}
		return year+month+day+pesel;
	}
	
	private String getRandomPhoneNumber(){
		String phonenumber = "";
		for(int i = 0; i < 9; i++) {
			int num = new Random().nextInt(10);
			if(i == 0 && num == 0){
				num = new Random().nextInt(10);
			}
			phonenumber += num;
			if(i % 3 == 2) {
				phonenumber += "-";
			}
		}
		return phonenumber.substring(0, phonenumber.length() - 1);
	}
	
	private String getRandomCCN(){
		String CCN = "";
		for(int i = 0; i < 16; i++) {
			int num = new Random().nextInt(10);
			if(i == 0 && num == 0){
				num = new Random().nextInt(10);
			}
			CCN += num;
			if(i % 4 == 3) {
				CCN += " ";
			}
		}
		return CCN.substring(0, CCN.length() - 1);
	}

	public int countLines(String filename) {
		int lines = 0;
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}

	public String getRandomLineFromFile(String filename) {
		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) + 1;
		int currLine = 1;
		String currentLineContent = "";
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				currLine++;
				currentLineContent = sc.nextLine();
				if (line == currLine) {
					return currentLineContent;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
