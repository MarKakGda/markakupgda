import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


public class HttpConnector {

    private URL obj;
    private HttpURLConnection con;
    private String userAgent;
    public  String sendGET(String url) throws IOException {
        obj = new URL(url);
        con = (HttpURLConnection) obj.openConnection();
        userAgent ="Marcin/1.0";
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent",userAgent);
        int responseCode = con.getResponseCode();
        String result = "";
        if(responseCode == 200){
            //pobieranie danych poprzez streamowaniex
            InputStream res = con.getInputStream();
            InputStreamReader reader = new InputStreamReader(res);
            Scanner sc = new Scanner(reader);
            while(sc.hasNextLine()){
                result += sc.nextLine();
            } sc.close();
            // System.out.println(result);
        }
        return result;
    }
    public String sendPOST(String url, String params) throws IOException {
        obj= new URL(url);
        con = (HttpURLConnection) obj.openConnection();
        userAgent = "Pawel/2.0";
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", userAgent);
        con.setDoOutput(true);
        DataOutputStream dos = new DataOutputStream(con.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();
//    koniec tylko dla POST
        Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
        String lines ="";
        if(sc.hasNextLine()){
            lines += sc.nextLine();
        } sc.close();
        return lines;
    }
}
