package mutabletypes;

import org.junit.Test;

public class MutableDoubleTest {
	
	@Test
	public void test() {
		MutableDouble a = new MutableDouble(10.0);
		MutableDouble b = a;

		
		assert a.getValue() == 10.0;
		assert b.getValue() == 10.0;
		
		a.setValue(100);
		
		assert a.getValue() == 100.0;
		assert b.getValue() == 100.0;
		
		a.decrement();
		
		assert a.getValue() == 99.0;
		assert b.getValue() == 99.0;
		
	}

}