<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<c:import url="header.jsp" />
<c:import url="sidebar.jsp" />
<c:import url="content2.jsp" />
<p>Imie: <c:out value="${imie}"/></p>
<ul>
    <c:forEach items="${}colors}" var="color">
        <li><c:out value="${color}"/></li>
    </c:forEach>
</ul>
<c:import url="footer.jsp" />
