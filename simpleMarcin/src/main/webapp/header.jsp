<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <title>Moja nowa strona</title>
    <link rel="stylesheet" href="<c:out value="style.css" />" />
</head>
<body>
<div class="container">
    <header class="header">
        <h1> To moja nowa strona </h1>
    </header>
    <br clear="both" />